package com.gl.authorityManage.model.vo;

import lombok.Data;

@Data
public class ScopeWithDescriptionVo {

    private String scope;

    private String description;

    public ScopeWithDescriptionVo(String scope, String description) {
        this.scope = scope;
        this.description = description;
    }

}
