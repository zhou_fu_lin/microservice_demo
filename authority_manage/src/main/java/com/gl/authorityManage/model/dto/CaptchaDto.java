package com.gl.authorityManage.model.dto;

import lombok.Data;

@Data
public class CaptchaDto {

    /**
     * 验证码id
     */
    private String captchaId;

    /**
     * 验证码的值
     */
    private String code;

    /**
     * 图片验证码的base64值
     */
    private String imageData;

    public CaptchaDto(String captchaId, String code, String imageData) {
        this.captchaId = captchaId;
        this.code = code;
        this.imageData = imageData;
    }

}
