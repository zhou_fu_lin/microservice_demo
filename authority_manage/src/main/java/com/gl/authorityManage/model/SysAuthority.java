package com.gl.authorityManage.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("sys_authority")
public class SysAuthority implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 菜单自增ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 上级ID
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 跳转URL
     */
    @TableField("url")
    private String url;

    /**
     * 所需权限
     */
    @TableField("authority")
    private String authority;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 0:菜单,1:接口
     */
    @TableField("type")
    private Integer type;

    /**
     * 0:启用,1:删除
     */
    @TableField("state")
    private Integer state;

    /**
     * 创建人
     */
    @TableField("create_user_id")
    private String createUserId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT, value = "create_time")
    private LocalDateTime createTime;

}
