package com.gl.authorityManage.model.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.DeferredSecurityContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolderStrategy;

import java.util.function.Supplier;

@Slf4j
public class SupplierDeferredSecurityContext implements DeferredSecurityContext {

    private final SecurityContextHolderStrategy strategy;

    private final Supplier<SecurityContext> supplier;

    private SecurityContext securityContext;

    public SupplierDeferredSecurityContext(SecurityContextHolderStrategy strategy, Supplier<SecurityContext> supplier) {
        this.strategy = strategy;
        this.supplier = supplier;
    }

    @Override
    public boolean isGenerated() {
        return init() == null;
    }

    @Override
    public SecurityContext get() {
        return init();
    }

    private SecurityContext init() {
        if (this.securityContext != null) {
            return this.securityContext;
        }

        this.securityContext = this.supplier.get();
        if (this.securityContext == null) {
            this.securityContext = this.strategy.createEmptyContext();
            log.info("Created {}", this.securityContext);
        }
        return this.securityContext;
    }
}
