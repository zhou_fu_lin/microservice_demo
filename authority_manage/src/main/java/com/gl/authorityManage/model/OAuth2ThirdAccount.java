package com.gl.authorityManage.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("oauth2_third_account")
public class OAuth2ThirdAccount implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 用户表主键
     */
    @TableField("user_id")
    private String userId;

    /**
     * 三方登录唯一id
     */
    @TableField("unique_id")
    private String uniqueId;

    /**
     * 三方登录用户名
     */
    @TableField("third_username")
    private String thirdUsername;

    /**
     * 三方登录获取的认证信息
     */
    @TableField("credentials")
    private String credentials;

    /**
     * 三方登录获取的认证信息的过期时间
     */
    @TableField("credentials_expires_at")
    private LocalDateTime credentialsExpiresAt;

    /**
     * 三方登录类型
     */
    @TableField("type")
    private String type;

    /**
     * 地址
     */
    @TableField("location")
    private String location;

    /**
     * 绑定时间
     */
    @TableField(fill = FieldFill.INSERT, value = "create_time")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE, value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 用户名、昵称
     */
    @TableField(exist = false)
    private String name;

    /**
     * 头像地址
     */
    @TableField(exist = false)
    private String avatarUrl;

}
