package com.gl.authorityManage.model.ao;

import lombok.Data;

/**
 * 扫描二维码入参
 *
 * @author vains
 */
@Data
public class QrCodeScanAo {

    /**
     * 二维码id
     */
    private String qrCodeId;

}
