package com.gl.authorityManage.model.po;

import lombok.Data;

/**
 * 生成二维码响应
 *
 * @author vains
 */
@Data
public class QrCodeGeneratePo {

    /**
     * 二维码ID
     */
    private String qrCodeId;

    /**
     * 二维码BASE64值(这里响应一个链接好一些)
     */
    private String imageData;

    public QrCodeGeneratePo() {}

    public QrCodeGeneratePo(String qrCodeId, String imageData) {
        this.qrCodeId = qrCodeId;
        this.imageData = imageData;
    }

}
