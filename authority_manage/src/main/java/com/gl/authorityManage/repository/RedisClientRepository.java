package com.gl.authorityManage.repository;

import com.gl.authorityManage.model.security.RedisRegisteredClient;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RedisClientRepository extends CrudRepository<RedisRegisteredClient, String> {

    /**
     * 根据客户端Id查询客户端信息
     *
     * @param clientId 客户端id
     * @return 客户端信息
     */
    Optional<RedisRegisteredClient> findByClientId(String clientId);

}
