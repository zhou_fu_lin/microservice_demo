package com.gl.authorityManage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gl.authorityManage.model.OAuth2ThirdAccount;

public interface OAuth2ThirdAccountService extends IService<OAuth2ThirdAccount> {

    void checkAndSaveUser(OAuth2ThirdAccount thirdAccount);

}
