//package com.gl.authorityManage.service.impl;
//
//import com.gl.authorityCommon.constants.SecurityConstant;
//import com.gl.authorityManage.authorization.wechat.WechatUserRequestEntityConverter;
//import com.gl.authorityManage.authorization.wechat.WechatUserResponseConverter;
//import com.gl.authorityManage.exception.InvalidCaptchaException;
//import com.gl.authorityManage.model.OAuth2ThirdAccount;
//import com.gl.authorityManage.service.OAuth2ThirdAccountService;
//import com.gl.authorityManage.component.OAuth2UserConverter;
//import jakarta.servlet.http.HttpServletRequest;
//import org.springframework.http.converter.ByteArrayHttpMessageConverter;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.converter.ResourceHttpMessageConverter;
//import org.springframework.http.converter.StringHttpMessageConverter;
//import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
//import org.springframework.security.authentication.AuthenticationServiceException;
//import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
//import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
//import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
//import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
//import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
//import org.springframework.security.oauth2.core.user.OAuth2User;
//import org.springframework.security.web.WebAttributes;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.context.request.RequestAttributes;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import java.util.LinkedHashMap;
//import java.util.List;
//
//@Service
//public class CustomOauth2UserServiceImpl extends DefaultOAuth2UserService {
//
//    private final OAuth2UserConverter oAuth2UserConverter;
//
//    private final OAuth2ThirdAccountService oAuth2thirdAccountService;
//
//    public CustomOauth2UserServiceImpl(OAuth2UserConverter oAuth2UserConverter, OAuth2ThirdAccountService oAuth2thirdAccountService) {
//        this.oAuth2UserConverter = oAuth2UserConverter;
//        this.oAuth2thirdAccountService = oAuth2thirdAccountService;
//        // 初始化时添加微信用户信息请求处理，OidcUserService本质上是调用该类获取用户信息的，不用添加
//        super.setRequestEntityConverter(new WechatUserRequestEntityConverter());
//
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.setErrorHandler(new OAuth2ErrorResponseErrorHandler());
//        List<HttpMessageConverter<?>> messageConverters = List.of(
//                new StringHttpMessageConverter(),
//                new ResourceHttpMessageConverter(),
//                new ByteArrayHttpMessageConverter(),
//                new AllEncompassingFormHttpMessageConverter(),
//                // 获取微信用户信息请求头支持text/plain
//                new WechatUserResponseConverter()
//        );
//        restTemplate.setMessageConverters(messageConverters);
//        super.setRestOperations(restTemplate);
//    }
//
//    @Override
//    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
//        try {
//            // 获取第三方用户信息
//            OAuth2User oAuth2User = super.loadUser(userRequest);
//            // 转换为项目第三方用户信息
//            OAuth2ThirdAccount oAuth2ThirdAccount = oAuth2UserConverter.convert(userRequest, oAuth2User);
//            // 检查用户信息
//            oAuth2thirdAccountService.checkAndSaveUser(oAuth2ThirdAccount);
//            // 将第三方登录类型设置到attributes中
//            LinkedHashMap<String, Object> attributes = new LinkedHashMap<>(oAuth2User.getAttributes());
//            // 将配置文件的registrationId作为类型
//            attributes.put(SecurityConstant.LOGIN_TYPE_NAME, userRequest.getClientRegistration().getRegistrationId());
//            String userNameAttributeName = userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName();
//            return new DefaultOAuth2User(oAuth2User.getAuthorities(), attributes, userNameAttributeName);
//        } catch (Exception e) {
//            // 获取当前request
//            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//            if (requestAttributes == null) {
//                throw new InvalidCaptchaException("Failed to get the current request.");
//            }
//            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
//            // 将异常放入session中
//            request.getSession(Boolean.FALSE).setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, e);
//            throw new AuthenticationServiceException(e.getMessage(), e);
//        }
//    }
//}
