//package com.gl.authorityManage.service;
//
//import com.gl.authorityManage.model.ao.QrCodeConsentAo;
//import com.gl.authorityManage.model.ao.QrCodeScanAo;
//import com.gl.authorityManage.model.po.QrCodeFetchPo;
//import com.gl.authorityManage.model.po.QrCodeGeneratePo;
//import com.gl.authorityManage.model.po.QrCodeScanPo;
//
///**
// * 二维码登录服务接口
// *
// * @author vains
// */
//public interface QrCodeService {
//
//    /**
//     * 生成二维码
//     *
//     * @return 二维码
//     */
//    QrCodeGeneratePo generateQrCode();
//
//    /**
//     * 扫描二维码响应
//     *
//     * @param scanInfo 二维码id
//     * @return 二维码信息
//     */
//    QrCodeScanPo scan(QrCodeScanAo scanInfo);
//
//    /**
//     * 二维码登录确认入参
//     *
//     * @param consentInfo 二维码id
//     */
//    void consent(QrCodeConsentAo consentInfo);
//
//    /**
//     * web端轮询二维码状态处理
//     *
//     * @param qrCodeId 二维码id
//     * @return 二维码信息
//     */
//    QrCodeFetchPo fetch(String qrCodeId);
//
//}
