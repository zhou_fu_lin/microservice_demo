//package com.gl.authorityManage.service.impl;
//
//import cn.hutool.core.util.StrUtil;
//import com.gl.authorityCommon.constants.SecurityConstant;
//import com.gl.authorityManage.model.OAuth2ThirdAccount;
//import com.gl.authorityManage.service.OAuth2ThirdAccountService;
//import com.gl.authorityManage.component.OAuth2UserConverter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
//import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
//import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
//import org.springframework.security.oauth2.core.oidc.OidcIdToken;
//import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
//import org.springframework.security.oauth2.core.oidc.user.OidcUser;
//import org.springframework.stereotype.Service;
//
//import java.util.LinkedHashMap;
//
//@Service
//public class CustomOidcUserServiceImpl extends OidcUserService {
//
//    @Autowired
//    private OAuth2UserConverter oAuth2UserConverter;
//
//    @Autowired
//    private OAuth2ThirdAccountService oAuth2ThirdAccountService;
//
//    @Override
//    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
//        // 获取第三方用户信息
//        OidcUser oidcUser = super.loadUser(userRequest);
//        // 转换成第三方用户信息
//        OAuth2ThirdAccount oAuth2ThirdAccount = oAuth2UserConverter.convert(userRequest, oidcUser);
//        // 检查用户信息
//        oAuth2ThirdAccountService.checkAndSaveUser(oAuth2ThirdAccount);
//        // 将第三方登录类型设置到attributes中，将配置文件的registrationId作为类型
//        OidcIdToken oidcIdToken = oidcUser.getIdToken();
//        LinkedHashMap<String, Object> attributes = new LinkedHashMap<>(oidcIdToken.getClaims());
//        attributes.put(SecurityConstant.LOGIN_TYPE_NAME, userRequest.getClientRegistration().getRegistrationId());
//        // 重新生成idToken
//        OidcIdToken newOidcIdToken = new OidcIdToken(oidcIdToken.getTokenValue(), oidcIdToken.getIssuedAt(), oidcIdToken.getExpiresAt(), attributes);
//        String userNameAttributeName = userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName();
//        // 重新生成oidcUser
//        if (StrUtil.isBlank(userNameAttributeName)) {
//            return new DefaultOidcUser(oidcUser.getAuthorities(), newOidcIdToken, oidcUser.getUserInfo(), userNameAttributeName);
//        }
//        return new DefaultOidcUser(oidcUser.getAuthorities(), newOidcIdToken, oidcUser.getUserInfo());
//    }
//}
