package com.gl.authorityManage.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gl.authorityManage.mapper.SysRoleAuthorityMapper;
import com.gl.authorityManage.mapper.SysUserRoleMapper;
import com.gl.authorityManage.mapper.SysAuthorityMapper;
import com.gl.authorityManage.model.SysAuthority;
import com.gl.authorityManage.model.SysRoleAuthority;
import com.gl.authorityManage.model.SysUserRole;
import com.gl.authorityManage.service.SysAuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SysAuthorityServiceImpl extends ServiceImpl<SysAuthorityMapper, SysAuthority> implements SysAuthorityService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private SysRoleAuthorityMapper sysRoleAuthorityMapper;

    @Override
    public List<SysAuthority> getByUserId(String userId) {
        /* 通过用户角色关联表查询对应的角色 */
        List<SysUserRole> userRoles = sysUserRoleMapper.selectList(Wrappers.lambdaQuery(SysUserRole.class).eq(SysUserRole::getUserId, userId));
        List<SysUserRole> sysUserRoles = Optional.ofNullable(userRoles).orElse(Collections.emptyList());
        List<String> rolesId = sysUserRoles.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        if (ObjectUtil.isEmpty(rolesId)) {
            return Collections.emptyList();
        }
        /* 通过角色菜单关联表查出对应的菜单 */
        List<SysRoleAuthority> sysRoleAuthority = sysRoleAuthorityMapper.selectList(Wrappers.lambdaQuery(SysRoleAuthority.class).in(SysRoleAuthority::getRoleId, rolesId));
        List<SysRoleAuthority> sysRoleAuthorityList = Optional.ofNullable(sysRoleAuthority).orElse(Collections.emptyList());
        List<String> authorityId = sysRoleAuthorityList.stream().map(SysRoleAuthority::getAuthorityId).collect(Collectors.toList());
        if (ObjectUtil.isEmpty(authorityId)) {
            return Collections.emptyList();
        }
        /* 根据菜单ID查出菜单 */
        return baseMapper.selectBatchIds(authorityId);
    }

}
