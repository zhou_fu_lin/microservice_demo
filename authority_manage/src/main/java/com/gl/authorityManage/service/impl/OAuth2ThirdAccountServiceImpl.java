package com.gl.authorityManage.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gl.authorityManage.mapper.OAuth2ThirdAccountMapper;
import com.gl.authorityManage.model.OAuth2ThirdAccount;
import com.gl.authorityManage.service.OAuth2ThirdAccountService;
import com.gl.authorityManage.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OAuth2ThirdAccountServiceImpl extends ServiceImpl<OAuth2ThirdAccountMapper, OAuth2ThirdAccount> implements OAuth2ThirdAccountService {

    @Autowired
    private SysUserService sysUserService;

    @Override
    public void checkAndSaveUser(OAuth2ThirdAccount thirdAccount) {
        // 构建第三方唯一标识和登录方式的查询条件
        OAuth2ThirdAccount oAuth2ThirdAccount = this.lambdaQuery()
                .eq(OAuth2ThirdAccount::getUniqueId, thirdAccount.getUniqueId())
                .eq(OAuth2ThirdAccount::getType, thirdAccount.getType())
                .one();
        // 不存在，进行保存
        if (ObjectUtil.isEmpty(oAuth2ThirdAccount)) {
            // 生成用户信息
            String userId = sysUserService.saveByThirdAccount(thirdAccount);
            thirdAccount.setUserId(userId);
            this.save(thirdAccount);
            return;
        }
        // 验证是否需要生成基础用户信息
        if (StrUtil.isBlank(oAuth2ThirdAccount.getUserId())) {
            String userId = sysUserService.saveByThirdAccount(thirdAccount);
            oAuth2ThirdAccount.setUserId(userId);
        }
        // 存在更新用户认证信息
        oAuth2ThirdAccount.setCredentialsExpiresAt(thirdAccount.getCredentialsExpiresAt());
        oAuth2ThirdAccount.setCredentials(thirdAccount.getCredentials());
        oAuth2ThirdAccount.setUpdateTime(null);
        this.updateById(oAuth2ThirdAccount);
    }

}
