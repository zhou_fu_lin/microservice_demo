package com.gl.authorityManage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gl.authorityManage.model.SysAuthority;

import java.util.List;

public interface SysAuthorityService extends IService<SysAuthority> {

    List<SysAuthority> getByUserId(String userId);

}
