package com.gl.authorityManage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gl.authorityManage.model.OAuth2ThirdAccount;
import com.gl.authorityManage.model.SysUser;

public interface SysUserService extends IService<SysUser> {

    String saveByThirdAccount(OAuth2ThirdAccount oAuth2ThirdAccount);

}
