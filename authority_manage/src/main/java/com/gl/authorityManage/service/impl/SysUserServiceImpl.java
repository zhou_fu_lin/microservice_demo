package com.gl.authorityManage.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gl.authorityManage.mapper.SysRoleAuthorityMapper;
import com.gl.authorityManage.mapper.SysUserRoleMapper;
import com.gl.authorityManage.mapper.SysAuthorityMapper;
import com.gl.authorityManage.mapper.SysUserMapper;
import com.gl.authorityManage.model.*;
import com.gl.authorityManage.model.security.CustomGrantedAuthority;
import com.gl.authorityManage.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService, UserDetailsService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private SysAuthorityMapper sysAuthorityMapper;

    @Autowired
    private SysRoleAuthorityMapper sysRoleAuthorityMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        /**
         *  在Security中“username”就代表了用户登录时输入的账号，在重写该方法时它可以代表以下内容：账号、手机号、邮箱、姓名等
         *  “username”在数据库中不一定非要是一样的列，它可以是手机号、邮箱，也可以都是，最主要的目的就是根据输入的内容获取到对应的用户信息，如下方所示
         *  通过传入的账号信息查询对应的用户信息
         */
        LambdaQueryWrapper<SysUser> wrapper = Wrappers.lambdaQuery(SysUser.class);
        wrapper.or(o -> o.eq(SysUser::getUsername, username));
        wrapper.or(o -> o.eq(SysUser::getEmail, username));
        wrapper.or(o -> o.eq(SysUser::getPhone, username));
        SysUser systemUser = baseMapper.selectOne(wrapper);
        if (systemUser == null) {
            throw new UsernameNotFoundException("账号不存在");
        }

        List<SysUserRole> userRoles = sysUserRoleMapper.selectList(Wrappers.lambdaQuery(SysUserRole.class).eq(SysUserRole::getUserId, systemUser.getId()));
        List<String> roleIds = Optional.ofNullable(userRoles).orElse(Collections.emptyList()).stream().map(SysUserRole::getRoleId).toList();
        if (ObjectUtil.isEmpty(roleIds)) {
            return systemUser;
        }

        /* 通过角色权限关联表查出对应的菜单 */
        List<SysRoleAuthority> roleMenus = sysRoleAuthorityMapper.selectList(Wrappers.lambdaQuery(SysRoleAuthority.class).in(SysRoleAuthority::getRoleId, roleIds));
        List<String> authorityIds = Optional.ofNullable(roleMenus).orElse(Collections.emptyList()).stream().map(SysRoleAuthority::getAuthorityId).toList();
        if (ObjectUtil.isEmpty(authorityIds)) {
            return systemUser;
        }

        /* 根据ID查询权限信息 */
        List<SysAuthority> authorities = sysAuthorityMapper.selectBatchIds(authorityIds);
        List<SysAuthority> authoritieList = Optional.ofNullable(authorities).orElse(Collections.emptyList());
        systemUser.setAuthorities(authoritieList.stream().map(SysAuthority::getAuthority).map(CustomGrantedAuthority::new).collect(Collectors.toSet()));
        return systemUser;
    }

    @Override
    public String saveByThirdAccount(OAuth2ThirdAccount oAuth2ThirdAccount) {
        SysUser sysUser = new SysUser();
        sysUser.setAvatarUrl(oAuth2ThirdAccount.getAvatarUrl());
        sysUser.setSourceFrom(oAuth2ThirdAccount.getType());
        sysUser.setName(oAuth2ThirdAccount.getName());
        sysUser.setState(Boolean.FALSE);
        this.save(sysUser);
        return sysUser.getId();
    }

}
