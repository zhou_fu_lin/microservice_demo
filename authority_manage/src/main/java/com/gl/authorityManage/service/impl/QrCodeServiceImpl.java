//package com.gl.authorityManage.service.impl;
//
//import cn.hutool.core.util.ObjectUtil;
//import cn.hutool.extra.qrcode.QrCodeUtil;
//import cn.hutool.extra.qrcode.QrConfig;
//import com.baomidou.mybatisplus.core.toolkit.IdWorker;
//import com.gl.authorityCommon.constants.RedisConstant;
//import com.gl.authorityManage.component.RedisOperator;
//import com.gl.authorityManage.model.SysUser;
//import com.gl.authorityManage.model.ao.QrCodeConsentAo;
//import com.gl.authorityManage.model.ao.QrCodeScanAo;
//import com.gl.authorityManage.model.po.QrCodeFetchPo;
//import com.gl.authorityManage.model.po.QrCodeGeneratePo;
//import com.gl.authorityManage.model.po.QrCodeScanPo;
//import com.gl.authorityManage.model.vo.QrCodeInfoVo;
//import com.gl.authorityManage.properties.CustomSecurityProperties;
//import com.gl.authorityManage.service.QrCodeService;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import jakarta.servlet.http.HttpSession;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.InsufficientAuthenticationException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.context.SecurityContextImpl;
//import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
//import org.springframework.security.oauth2.core.OAuth2Error;
//import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
//import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
//import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
//import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
//import org.springframework.security.web.savedrequest.DefaultSavedRequest;
//import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
//import org.springframework.security.web.savedrequest.RequestCache;
//import org.springframework.security.web.util.UrlUtils;
//import org.springframework.stereotype.Service;
//import org.springframework.util.Assert;
//import org.springframework.web.context.request.RequestAttributes;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import java.security.Principal;
//import java.time.LocalDateTime;
//import java.util.Set;
//
//import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;
//
///**
// * 二维码登录接口实现
// *
// * @author vains
// */
//@Slf4j
//@Service
//public class QrCodeServiceImpl implements QrCodeService {
//
//    @Autowired
//    private RedisOperator<QrCodeInfoVo> redisOperator;
//
//    @Autowired
//    private RedisOperator<String> stringRedisOperator;
//
//    @Autowired
//    private CustomSecurityProperties customSecurityProperties;
//
//    @Autowired
//    private RedisOAuth2AuthorizationService authorizationService;
//
//    @Autowired
//    private RedisOperator<UsernamePasswordAuthenticationToken> authenticationRedisOperator;
//
//    private final RequestCache requestCache = new HttpSessionRequestCache();
//
//    @Override
//    public QrCodeGeneratePo generateQrCode() {
//        // 生成二维码ID
//        String qrCodeId = IdWorker.getIdStr();
//        // 生成二维码并转为BASE64
//        String pngQrCode = QrCodeUtil.generateAsBase64(qrCodeId, new QrConfig(), "png");
//
//        LocalDateTime timeOut = LocalDateTime.now().plusMinutes(2L);
//        QrCodeInfoVo info = new QrCodeInfoVo();
//        info.setExpiresTime(timeOut);
//        info.setQrCodeId(qrCodeId);
//        info.setQrCodeStatus(0);
//
//        // 获取当前request
//        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//        if (requestAttributes == null) {
//            // 上边设置的过期时间是2分钟，这里设置10分钟过期，可根据业务自行调整过期时间
//            redisOperator.set(RedisConstant.QR_CODE_PREV + qrCodeId, info, RedisConstant.QR_CODE_INFO_TIMEOUT);
//            return new QrCodeGeneratePo(qrCodeId, pngQrCode);
//        }
//
//        // 获取当前session
//        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
//        HttpServletResponse response = ((ServletRequestAttributes) requestAttributes).getResponse();
//        DefaultSavedRequest savedRequest = (DefaultSavedRequest) this.requestCache.getRequest(request, response);
//        if (savedRequest == null) {
//            // 上边设置的过期时间是2分钟，这里设置10分钟过期，可根据业务自行调整过期时间
//            redisOperator.set(RedisConstant.QR_CODE_PREV + qrCodeId, info, RedisConstant.QR_CODE_INFO_TIMEOUT);
//            return new QrCodeGeneratePo(qrCodeId, pngQrCode);
//        }
//
//        if (!UrlUtils.isAbsoluteUrl(customSecurityProperties.getLoginUrl())) {
//            // 获取查询参数与请求路径
//            String queryString = savedRequest.getQueryString();
//            String requestUri = savedRequest.getRequestURI();
//            // 前后端不分离根据请求和请求参数跳转
//            info.setBeforeLoginQueryString(queryString);
//            info.setBeforeLoginQueryString(requestUri);
//        }
//
//        // 获取跳转登录之前访问URL的参数
//        String[] scopes = savedRequest.getParameterValues("scope");
//        if (!ObjectUtil.isEmpty(scopes)) {
//            // 不为空时，获取数组首值并设置进二维码信息中
//            Set<String> scope = Set.of(scopes[0].split(" "));
//            info.setScopes(scope);
//        }
//
//        redisOperator.set(RedisConstant.QR_CODE_PREV + qrCodeId, info, RedisConstant.QR_CODE_INFO_TIMEOUT);
//        return new QrCodeGeneratePo(qrCodeId, pngQrCode);
//    }
//
//    @Override
//    public QrCodeScanPo scan(QrCodeScanAo scanInfo) {
//        // 应该用validation的
//        Assert.hasLength(scanInfo.getQrCodeId(), "二维码Id不能为空.");
//
//        // 校验二维码状态
//        QrCodeInfoVo info = redisOperator.get(RedisConstant.QR_CODE_PREV + scanInfo.getQrCodeId());
//        if (info == null) {
//            throw new RuntimeException("无效二维码.");
//        }
//
//        // 验证状态
//        if (!ObjectUtil.equals(info.getQrCodeStatus(), 0)) {
//            throw new RuntimeException("二维码已被其他人扫描，无法重复扫描.");
//        }
//
//        // 二维码是否过期
//        boolean qrCodeExpire = info.getExpiresTime().isBefore(LocalDateTime.now());
//        if (qrCodeExpire) {
//            throw new RuntimeException("二维码已过期.");
//        }
//
//        // 获取登录用户信息
//        QrCodeScanPo scanInfoPo = new QrCodeScanPo();
//        OAuth2Authorization oAuth2Authorization = getOAuth2Authorization();
//        if (oAuth2Authorization == null) {
//            OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.INVALID_TOKEN, "登录已过期.", null);
//            throw new OAuth2AuthenticationException(error);
//        }
//        // APP端使用密码模式、手机认证登录，不使用三方登录的情况
//        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = oAuth2Authorization.getAttribute(Principal.class.getName());
//        if (usernamePasswordAuthenticationToken.getPrincipal() instanceof SysUser basicUser) {
//            // 生成临时票据
//            String qrCodeTicket = IdWorker.getIdStr();
//            // 根据二维码id和临时票据存储，确认时根据临时票据认证
//            String redisQrCodeTicketKey = String.format("%s%s:%s", RedisConstant.QR_CODE_PREV, scanInfo.getQrCodeId(), qrCodeTicket);
//            stringRedisOperator.set(redisQrCodeTicketKey, qrCodeTicket, RedisConstant.QR_CODE_INFO_TIMEOUT);
//
//            // 更新二维码信息的状态
//            info.setQrCodeStatus(1);
//            info.setName(basicUser.getName());
//            info.setAvatarUrl(basicUser.getAvatarUrl());
//            redisOperator.set(RedisConstant.QR_CODE_PREV + scanInfo.getQrCodeId(), info, RedisConstant.QR_CODE_INFO_TIMEOUT);
//
//            // 封装响应
//            scanInfoPo.setQrCodeTicket(qrCodeTicket);
//            scanInfoPo.setScopes(info.getScopes());
//            scanInfoPo.setExpired(Boolean.FALSE);
//            scanInfoPo.setQrCodeStatus(0);
//        }
//
//        // 其它登录方式暂不处理
//        return scanInfoPo;
//    }
//
//    @Override
//    public void consent(QrCodeConsentAo consentInfo) {
//        // 应该用Validation
//        Assert.hasLength(consentInfo.getQrCodeId(), "二维码Id不能为空.");
//
//        // 校验二维码状态
//        QrCodeInfoVo info = redisOperator.get(RedisConstant.QR_CODE_PREV + consentInfo.getQrCodeId());
//        if (info == null) {
//            throw new RuntimeException("无效二维码或二维码已过期.");
//        }
//
//        // 验证临时票据
//        String qrCodeTicketKey = String.format("%s%s:%s", RedisConstant.QR_CODE_PREV, consentInfo.getQrCodeId(), consentInfo.getQrCodeTicket());
//        String redisQrCodeTicket = stringRedisOperator.get(qrCodeTicketKey);
//        if (!ObjectUtil.equals(redisQrCodeTicket, consentInfo.getQrCodeTicket())) {
//            // 临时票据有误、临时票据失效(超过redis存活时间后确认)、redis数据有误
//            log.debug("临时票据有误、临时票据失效(超过redis存活时间后确认)、redis数据有误.");
//            throw new RuntimeException("登录确认失败，请重新扫描.");
//        }
//        // 使用后删除
//        stringRedisOperator.delete(qrCodeTicketKey);
//
//        // 获取登录用户信息
//        OAuth2Authorization authorization = this.getOAuth2Authorization();
//        if (authorization == null) {
//            throw new OAuth2AuthenticationException(new OAuth2Error(OAuth2ErrorCodes.INVALID_TOKEN, "登录已过期.", null));
//        }
//
//        // app端使用密码模式、手机认证登录，不使用三方登录的情况
//        UsernamePasswordAuthenticationToken authenticationToken = authorization.getAttribute(Principal.class.getName());
//
//        // 根据二维码id存储用户信息
//        String redisUserinfoKey = String.format("%s%s:%s", RedisConstant.QR_CODE_PREV, "userinfo", consentInfo.getQrCodeId());
//        // 存储用户信息
//        authenticationRedisOperator.set(redisUserinfoKey, authenticationToken, RedisConstant.QR_CODE_INFO_TIMEOUT);
//
//        // 更新二维码信息的状态
//        info.setQrCodeStatus(2);
//        redisOperator.set(RedisConstant.QR_CODE_PREV + consentInfo.getQrCodeId(), info, RedisConstant.QR_CODE_INFO_TIMEOUT);
//    }
//
//    @Override
//    public QrCodeFetchPo fetch(String qrCodeId) {
//        // 校验二维码状态
//        QrCodeInfoVo info = redisOperator.get(RedisConstant.QR_CODE_PREV + qrCodeId);
//        if (info == null) {
//            throw new RuntimeException("二维码无效或二维码已过期.");
//        }
//
//        QrCodeFetchPo fetchInfoPo = new QrCodeFetchPo();
//        // 设置二维码是否过期、状态
//        fetchInfoPo.setQrCodeStatus(info.getQrCodeStatus());
//        fetchInfoPo.setExpired(info.getExpiresTime().isBefore(LocalDateTime.now()));
//
//        if (!ObjectUtil.equals(info.getQrCodeStatus(), 0)) {
//            // 如果是已扫描/已确认
//            fetchInfoPo.setName(info.getName());
//            fetchInfoPo.setAvatarUrl(info.getAvatarUrl());
//        }
//
//        // 如果不是已确认，直接返回
//        if (!ObjectUtil.equals(info.getQrCodeStatus(), 2)) {
//            return fetchInfoPo;
//        }
//
//        // 根据二维码ID从redis获取用户信息放入当前session中
//        String redisUserinfoKey = String.format("%s%s:%s", RedisConstant.QR_CODE_PREV, "userinfo", qrCodeId);
//        UsernamePasswordAuthenticationToken authenticationToken = authenticationRedisOperator.get(redisUserinfoKey);
//        if (authenticationToken == null) {
//            throw new RuntimeException("获取登录确认用户信息失败.");
//        }
//
//        // 获取当前request
//        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//        if (requestAttributes == null) {
//            throw new RuntimeException("获取当前Request失败.");
//        }
//        // 获取当前session
//        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
//        HttpSession session = request.getSession(Boolean.FALSE);
//        if (session != null) {
//            // 获取到认证信息后将之前扫码确认的用户信息放入当前session中。
//            session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, new SecurityContextImpl(authenticationToken));
//
//            // 操作成功后移除缓存
//            redisOperator.delete(RedisConstant.QR_CODE_PREV + qrCodeId);
//            // 删除用户信息，防止其它人重放请求
//            authenticationRedisOperator.delete(redisUserinfoKey);
//
//            // 填充二维码数据，设置跳转到登录之前的请求路径、查询参数和是否授权申请请求
//            fetchInfoPo.setBeforeLoginRequestUri(info.getBeforeLoginRequestUri());
//            fetchInfoPo.setBeforeLoginQueryString(info.getBeforeLoginQueryString());
//        }
//
//        return fetchInfoPo;
//    }
//
//    /**
//     * 获取当前使用token对应的认证信息
//     *
//     * @return oauth2认证信息
//     */
//    private OAuth2Authorization getOAuth2Authorization() {
//        // 校验登录状态
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication == null) {
//            throw new InsufficientAuthenticationException("未登录.");
//        }
//        if (authentication instanceof JwtAuthenticationToken jwtToken) {
//            // jwt处理
//            String tokenValue = jwtToken.getToken().getTokenValue();
//            // 根据token获取授权登录时的认证信息(登录用户)
//            return authorizationService.findByToken(tokenValue, OAuth2TokenType.ACCESS_TOKEN);
//        }
//        return null;
//    }
//
//}
