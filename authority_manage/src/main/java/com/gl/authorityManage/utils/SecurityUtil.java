package com.gl.authorityManage.utils;

import cn.hutool.core.collection.CollectionUtil;
import com.gl.authorityCommon.utils.JsonUtil;
import com.gl.authorityManage.authorization.handler.LoginTargetAuthenticationEntryPoint;
import com.gl.authorityManage.properties.CustomSecurityProperties;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientAuthenticationToken;
import org.springframework.security.oauth2.server.resource.BearerTokenError;
import org.springframework.security.oauth2.server.resource.BearerTokenErrorCodes;
import org.springframework.security.oauth2.server.resource.authentication.AbstractOAuth2TokenAuthenticationToken;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.filter.CorsFilter;

import java.io.IOException;
import java.util.*;

@Slf4j
public class SecurityUtil {

    /**
     * 从认证信息中获取客户端token
     */
    public static OAuth2ClientAuthenticationToken getAuthenticatedClient(Authentication authentication) {
        OAuth2ClientAuthenticationToken clientPrincipal = null;
        if (OAuth2ClientAuthenticationToken.class.isAssignableFrom(authentication.getPrincipal().getClass())) {
            clientPrincipal = (OAuth2ClientAuthenticationToken) authentication.getPrincipal();
        }
        if (clientPrincipal != null && clientPrincipal.isAuthenticated()) {
            return clientPrincipal;
        }
        throw new OAuth2AuthenticationException(OAuth2ErrorCodes.INVALID_CLIENT);
    }

    /**
     * 提取请求中的参数并转为一个map返回
     */
    public static MultiValueMap<String, String> getParameters(HttpServletRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        MultiValueMap<String, String> parameter = new LinkedMultiValueMap<>();
        parameterMap.forEach((key, values) -> {
            for (String value : values) {
                parameter.add(key, value);
            }
        });
        return parameter;
    }

    /**
     * 抛出 OAuth2AuthenticationException 异常
     */
    public static void throwOAuth2Error(String errorCode, String message, String errorUri) {
        OAuth2Error error = new OAuth2Error(errorCode, message, errorUri);
        throw new OAuth2AuthenticationException(error);
    }

    /**
     * 认证与鉴权失败回调
     */
    public static void exceptionHandler(HttpServletRequest request, HttpServletResponse response, Throwable e) {
        Map<String, String> parameter = getErrorParameter(request, response, e);
        String wwwAuthenticate = wwwAuthenticateHeaderValue(parameter);
        response.addHeader(HttpHeaders.WWW_AUTHENTICATE, wwwAuthenticate);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        try {
            response.getWriter().write(JsonUtil.objectCovertToJson(parameter));
            response.getWriter().flush();
        } catch (IOException ex) {
            log.error("写回错误信息失败", e);
        }
    }

    public static Map<String, String> getErrorParameter(HttpServletRequest request, HttpServletResponse response, Throwable e) {
        Map<String, String> parameter = new LinkedHashMap<String, String>();
        parameter.put("message", e.getMessage());
        /* 权限不足 */
        if (request.getUserPrincipal() instanceof AbstractOAuth2TokenAuthenticationToken) {
            parameter.put("error", BearerTokenErrorCodes.INSUFFICIENT_SCOPE);
            parameter.put("error_description", "The request requires higher privileges than provided by the access token.");
            parameter.put("error_uri", "https://tools.ietf.org/html/rfc6750#section-3.1");
            response.setStatus(HttpStatus.FORBIDDEN.value());
        }
        /* 没有携带JWT访问接口，没有客户端认证信息 */
        if (e instanceof InsufficientAuthenticationException) {
            parameter.put("error", BearerTokenErrorCodes.INVALID_TOKEN);
            parameter.put("error_description", "Not authorized.");
            parameter.put("error_uri", "https://tools.ietf.org/html/rfc6750#section-3.1");
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        /* jwt异常，JWT超过有效期、无效等 */
        if (e instanceof OAuth2AuthenticationException authenticationException) {
            OAuth2Error error = authenticationException.getError();
            parameter.put("error", error.getErrorCode());
            parameter.put("error_uri", error.getUri());
            parameter.put("error_description", error.getDescription());
            if (!(error instanceof BearerTokenError bearerTokenError)) {
                return parameter;
            }
            parameter.put("scope", bearerTokenError.getScope());
            response.setStatus(bearerTokenError.getHttpStatus().value());
        }
        return parameter;
    }

    /**
     * 生成放入请求头的错误信息
     */
    public static String wwwAuthenticateHeaderValue(Map<String, String> parameter) {
        StringBuilder wwwAuthenticate = new StringBuilder();
        wwwAuthenticate.append("Bearer");
        if (CollectionUtil.isEmpty(parameter)) {
            return wwwAuthenticate.toString();
        }
        wwwAuthenticate.append(" ");
        Set<Map.Entry<String, String>> entrySet = parameter.entrySet();
        List<Map.Entry<String, String>> entryList = new ArrayList<>(entrySet);
        for (int i = 0; i < entryList.size(); i++) {
            Map.Entry<String, String> entry = entryList.get(i);
            wwwAuthenticate.append(entry.getKey()).append("=\"").append(entry.getValue()).append("\"");
            wwwAuthenticate.append((i != parameter.size() - 1) ? ", " : "");
        }
        return wwwAuthenticate.toString();
    }

    public static void applyBasicSecurity(HttpSecurity http, CorsFilter corsFilter, CustomSecurityProperties customSecurityProperties) throws Exception {
        // 添加跨域过滤器
        http.addFilter(corsFilter);
        // 禁用 csrf 与 cors
        http.csrf(AbstractHttpConfigurer::disable);
        http.cors(AbstractHttpConfigurer::disable);

        /* 自定义未登录处理，兼容前后端不分离方式 */
        http.exceptionHandling(exceptionHandlingCustomizer -> exceptionHandlingCustomizer.defaultAuthenticationEntryPointFor(
            /* 当未登录，访问认证断点时，重定向至login页页面 */
            new LoginTargetAuthenticationEntryPoint(customSecurityProperties.getLoginUrl(), customSecurityProperties.getDeviceActivateUri()),
            new MediaTypeRequestMatcher(MediaType.TEXT_HTML)
        ));

        /* 处理使用accessToken访问用户信息端点和客户端注册端点 */
        http.oauth2ResourceServer((oauth2ResourceServerCustomizer) -> {
            oauth2ResourceServerCustomizer.jwt(Customizer.withDefaults());
            /* 添加BearerTokenAuthenticationFilter, 将认证服务当作资源服务，解析请求头的token(移动到资源服务) */
            oauth2ResourceServerCustomizer.accessDeniedHandler(SecurityUtil::exceptionHandler);
            oauth2ResourceServerCustomizer.authenticationEntryPoint(SecurityUtil::exceptionHandler);
        });
    }

}
