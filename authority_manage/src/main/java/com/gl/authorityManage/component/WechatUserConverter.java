//package com.gl.authorityManage.component;
//
//import com.gl.authorityCommon.constants.SecurityConstant;
//import com.gl.authorityManage.model.OAuth2ThirdAccount;
//import com.gl.authorityManage.strategy.OAuth2UserConverterStrategy;
//import org.springframework.security.oauth2.core.user.OAuth2User;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
///**
// * 微信用户信息转换器
// */
//@Component(SecurityConstant.THIRD_LOGIN_WECHAT)
//public class WechatUserConverter implements OAuth2UserConverterStrategy {
//
//    @Override
//    public OAuth2ThirdAccount convert(OAuth2User oAuth2User) {
//        /* 获取第三方用户信息 */
//        Map<String, Object> attributes = oAuth2User.getAttributes();
//        /* 转换成OAuth2ThirdAccount */
//        OAuth2ThirdAccount oAuth2ThirdAccount = new OAuth2ThirdAccount();
//        oAuth2ThirdAccount.setType(SecurityConstant.THIRD_LOGIN_WECHAT);
//        oAuth2ThirdAccount.setThirdUsername(oAuth2User.getName());
//        oAuth2ThirdAccount.setName(oAuth2User.getName());
//
//        String openid = String.valueOf(attributes.get("openid"));
//        oAuth2ThirdAccount.setUniqueId(openid);
//
//        Object city = attributes.get("city");
//        Object province = attributes.get("province");
//        oAuth2ThirdAccount.setLocation(province + "" + city);
//
//        String headImgUrl = String.valueOf(attributes.get("headimgurl"));
//        oAuth2ThirdAccount.setAvatarUrl(headImgUrl);
//        return oAuth2ThirdAccount;
//    }
//
//}
