//package com.gl.authorityManage.component;
//
//import cn.hutool.core.util.StrUtil;
//import com.gl.authorityManage.model.OAuth2ThirdAccount;
//import com.gl.authorityManage.strategy.OAuth2UserConverterStrategy;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeansException;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
//import org.springframework.security.oauth2.core.OAuth2AccessToken;
//import org.springframework.security.oauth2.core.user.OAuth2User;
//import org.springframework.stereotype.Component;
//
//import java.time.Instant;
//import java.time.LocalDateTime;
//import java.time.ZoneId;
//
///**
// * 调用第三方策略信息处理
// */
//@Slf4j
//@Component
//public class OAuth2UserConverter implements ApplicationContextAware {
//
//    /**
//     * 注入所有实例，map的key是实例在ioc中的名字
//     * 这里通过构造器注入所有Oauth2UserConverterStrategy的实例，在编译时已经通过
//     */
//    private ApplicationContext applicationContext;
//
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        this.applicationContext = applicationContext;
//    }
//
//    /**
//     * 获取转换器实例
//     */
//    public OAuth2UserConverterStrategy getInstance(String loginType) {
//        if (StrUtil.isBlank(loginType)) {
//            throw new UnsupportedOperationException("登录方法不能为空");
//        }
//        try {
//            return applicationContext.getBean(loginType, OAuth2UserConverterStrategy.class);
//        } catch (Exception e) {
//            log.error("OAuth2UserConverterContext-getInstance: 获取Bean对象失败");
//            throw new UnsupportedOperationException("不支持[" + loginType + "]第三方的用户信息转换器");
//        }
//    }
//
//    /**
//     * 根据登录方式获取转换器实例，使用转换器获取用户信息
//     *
//     * @param oAuth2UserRequest 获取第三方用户信息入参
//     * @param oAuth2User 第三方登录的认证信息
//     */
//    public OAuth2ThirdAccount convert(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
//        // 获取第三方登录配置的registrationId
//        String registrationId = oAuth2UserRequest.getClientRegistration().getRegistrationId();
//        // 转换用户信息
//        OAuth2ThirdAccount oAuth2ThirdAccount = this.getInstance(registrationId).convert(oAuth2User);
//        // 获取AccessToken
//        OAuth2AccessToken oAuth2AccessToken = oAuth2UserRequest.getAccessToken();
//        // 设置token
//        oAuth2ThirdAccount.setCredentials(oAuth2AccessToken.getTokenValue());
//        // 设置账号方式
//        oAuth2ThirdAccount.setType(registrationId);
//        Instant expiresAt = oAuth2AccessToken.getExpiresAt();
//        if (expiresAt != null) {
//            LocalDateTime tokenExpiresAt = expiresAt.atZone(ZoneId.of("UTC")).toLocalDateTime();
//            // token过期时间
//            oAuth2ThirdAccount.setCredentialsExpiresAt(tokenExpiresAt);
//        }
//        return oAuth2ThirdAccount;
//    }
//}
