//package com.gl.authorityManage.component;
//
//import com.gl.authorityCommon.constants.SecurityConstant;
//import com.gl.authorityManage.model.OAuth2ThirdAccount;
//import com.gl.authorityManage.strategy.OAuth2UserConverterStrategy;
//import org.springframework.security.oauth2.core.user.OAuth2User;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
///**
// * 转换通过码云登录的用户信息
// */
//@Component(SecurityConstant.THIRD_LOGIN_GITEE)
//public class GiteeUserConverter implements OAuth2UserConverterStrategy {
//
//    @Override
//    public OAuth2ThirdAccount convert(OAuth2User oAuth2User) {
//        /* 获取三方用户信息 */
//        Map<String, Object> attributes = oAuth2User.getAttributes();
//        /* 转换为OAuth2ThirdAccount */
//        OAuth2ThirdAccount oAuth2ThirdAccount = new OAuth2ThirdAccount();
//        oAuth2ThirdAccount.setType(SecurityConstant.THIRD_LOGIN_GITEE);
//        oAuth2ThirdAccount.setUniqueId(oAuth2User.getName());
//
//        String login = String.valueOf(attributes.get("login"));
//        oAuth2ThirdAccount.setThirdUsername(login);
//        /* 设置基础信息 */
//        String avatarUrl = String.valueOf(attributes.get("avatar_url"));
//        String name = String.valueOf(attributes.get("name"));
//        oAuth2ThirdAccount.setAvatarUrl(avatarUrl);
//        oAuth2ThirdAccount.setName(name);
//        return oAuth2ThirdAccount;
//    }
//}
