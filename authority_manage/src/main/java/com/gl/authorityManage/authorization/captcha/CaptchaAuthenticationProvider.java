package com.gl.authorityManage.authorization.captcha;

import com.gl.authorityCommon.constants.SecurityConstant;
import com.gl.authorityManage.component.RedisOperator;
import com.gl.authorityManage.exception.InvalidCaptchaException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 验证码校验(需要添加@Component注解)
 * 注入ioc中替换原先的DaoAuthenticationProvider
 * 在authenticate方法中添加校验验证码的逻辑
 * 最后调用父类的authenticate方法并返回
 */
@Slf4j
@Component
public class CaptchaAuthenticationProvider extends DaoAuthenticationProvider {

    private final RedisOperator<String> redisOperator;

    private final Map<String, CaptchaStrategyProvider> strategyMap = new LinkedHashMap<>();

    /**
     * 利用构造方法在通过@Component注解初始化时
     * 注入UserDetailsService和passwordEncoder，然后
     * 设置调用父类关于这两个属性的set方法设置进去
     *
     * @param userDetailsService 用户服务，给框架提供用户信息
     * @param passwordEncoder 密码解析器，用于加密和校验密码
     */
    public CaptchaAuthenticationProvider(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder, RedisOperator<String> redisOperator) {
        this.strategyMap.put(SecurityConstant.LOGIN_TYPE_PASSWORD, new GraphCaptchaStrategyProvider());
//        this.strategyMap.put(SecurityConstant.GRANT_TYPE_SMS_CODE, new SmsCaptchaStrategyProvider());
//        this.strategyMap.put(SecurityConstant.LOGIN_TYPE_SMS, new SmsCaptchaStrategyProvider());

        super.setUserDetailsService(userDetailsService);
        super.setPasswordEncoder(passwordEncoder);
        this.redisOperator = redisOperator;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (authentication.getCredentials() == null) {
            log.debug("Failed to authenticate since no credentials provided");
            throw new BadCredentialsException("The sms captcha cannot be empty.");
        }

        /* 获取当前request */
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            throw new InvalidCaptchaException("Failed to get the current request.");
        }
        CaptchaStrategyProvider strategy = getStrategy(requestAttributes);
        if (strategy.additionalAuthenticationChecks(userDetails, authentication, this.redisOperator)) {
            /* 执行其它拓展登录方式，比如邮箱登录什么的，父类默认实现的密码方式登录 */
            super.additionalAuthenticationChecks(userDetails, authentication);
        }
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        /* 获取当前request */
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            throw new InvalidCaptchaException("Failed to get the current request.");
        }

        CaptchaStrategyProvider strategy = getStrategy(requestAttributes);
        Authentication strategyValue = strategy.authenticate(authentication, this.redisOperator);

        log.info("Captcha authenticated.");
        return super.authenticate(strategyValue);
    }

    private CaptchaStrategyProvider getStrategy(RequestAttributes requestAttributes) {
        /* 获取当前登录方式，并执行验证逻辑 */
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        String loginType = request.getParameter(SecurityConstant.LOGIN_TYPE_NAME);
        CaptchaStrategyProvider strategy = this.strategyMap.get(loginType);
        /* 获取GRANT_TYPE */
        String grantType = request.getParameter("grant_type");
        return (strategy == null) ? this.strategyMap.get(grantType) : strategy;
    }

}
