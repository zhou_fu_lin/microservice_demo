package com.gl.authorityManage.authorization.handler;

import com.gl.authorityCommon.model.po.Result;
import com.gl.authorityCommon.utils.JsonUtil;
import com.gl.authorityCommon.utils.ResultUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 登录成功处理类
 */
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        Result<String> success = ResultUtil.success();
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        String jsonStr = JsonUtil.objectCovertToJson(success);
        response.getWriter().write(jsonStr);
        response.getWriter().flush();
    }

}
