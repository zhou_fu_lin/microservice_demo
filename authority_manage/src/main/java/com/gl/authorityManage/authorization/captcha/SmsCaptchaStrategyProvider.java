//package com.gl.authorityManage.authorization.captcha;
//
//import cn.hutool.core.util.ObjectUtil;
//import com.gl.authorityCommon.constants.RedisConstant;
//import com.gl.authorityManage.component.RedisOperator;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.userdetails.UserDetails;
//
//@Slf4j
//public class SmsCaptchaStrategyProvider implements CaptchaStrategyProvider {
//
//    @Override
//    public Boolean additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication, RedisOperator<String> redisOperator) {
//        log.info("Authenticate sms captcha...");
//
//        if (authentication.getCredentials() == null) {
//            log.debug("Failed to authenticate since no credentials provided");
//            throw new BadCredentialsException("The sms captcha cannot be empty.");
//        }
//        /* 获取存入session的验证码(UsernamePasswordAuthenticationToken的principal中现在存入的是手机号) */
//        String phone = (String) authentication.getPrincipal();
//        String smsCaptcha = redisOperator.getAndDelete(RedisConstant.SMS_CAPTCHA_PREFIX_KEY + phone);
//        /* 校验输入的验证码是否正确(UsernamePasswordAuthenticationToken的credentials中现在存入的是输入的验证码) */
//        if (!ObjectUtil.equals(smsCaptcha, authentication.getCredentials())) {
//            throw new BadCredentialsException("The sms captcha is incorrect.");
//        }
//        log.info("Authenticated sms captcha.");
//        return false;
//    }
//
//    @Override
//    public Authentication authenticate(Authentication authentication, RedisOperator<String> redisOperator) {
//        log.info("Authenticate sms captcha, No authentication is required.");
//        return authentication;
//    }
//}
