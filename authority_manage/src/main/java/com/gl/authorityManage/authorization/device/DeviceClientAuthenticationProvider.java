//package com.gl.authorityManage.authorization.device;
//
//import com.gl.authorityCommon.constants.ConfigConstant;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
//import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
//import org.springframework.security.oauth2.core.OAuth2Error;
//import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
//import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
//import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
//import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
//
//import java.util.Set;
//
///**
// * 设备码认证提供者
// */
//@Slf4j
//public class DeviceClientAuthenticationProvider implements AuthenticationProvider {
//
//    private final RegisteredClientRepository registeredClientRepository;
//
//    public DeviceClientAuthenticationProvider(RegisteredClientRepository registeredClientRepository) {
//        this.registeredClientRepository = registeredClientRepository;
//    }
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        /* 执行设备码流程 */
//        DeviceClientAuthenticationToken token = (DeviceClientAuthenticationToken) authentication;
//
//        /* 只支持公共客户端 */
//        ClientAuthenticationMethod methodTypeByNone = ClientAuthenticationMethod.NONE;
//        ClientAuthenticationMethod methodType = token.getClientAuthenticationMethod();
//        if (!methodTypeByNone.equals(methodType)) {
//            return null;
//        }
//
//        /* 获取客户端ID并查询校验 */
//        String clientId = token.getPrincipal().toString();
//        RegisteredClient registeredClient = this.registeredClientRepository.findByClientId(clientId);
//        if (registeredClient == null) {
//            String message = "Device client authentication failed: " + OAuth2ParameterNames.CLIENT_ID;
//            OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.INVALID_CLIENT, message, ConfigConstant.ERROR_URI_3_2_1);
//            throw new OAuth2AuthenticationException(error);
//        }
//
//        if (log.isTraceEnabled()) {
//            log.trace("Retrieved registered client");
//        }
//
//        /* 校验客户端 */
//        Set<ClientAuthenticationMethod> methodTypeSet = registeredClient.getClientAuthenticationMethods();
//        if (!methodTypeSet.contains(token.getClientAuthenticationMethod())) {
//            String message = "Device client authentication failed: authentication_method";
//            OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.INVALID_CLIENT, message, ConfigConstant.ERROR_URI_3_2_1);
//            throw new OAuth2AuthenticationException(error);
//        }
//
//        if (log.isTraceEnabled()) {
//            log.trace("Validated device client authentication parameters");
//        }
//
//        if (log.isTraceEnabled()) {
//            log.trace("Authenticated device client");
//        }
//
//
//        return new DeviceClientAuthenticationToken(registeredClient, methodType, null);
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return DeviceClientAuthenticationToken.class.isAssignableFrom(authentication);
//    }
//}
