package com.gl.authorityManage.authorization.captcha;

import com.gl.authorityManage.component.RedisOperator;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

public interface CaptchaStrategyProvider {

    Boolean additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication, RedisOperator<String> redisOperator);

    Authentication authenticate(Authentication authentication, RedisOperator<String> redisOperator);

}
