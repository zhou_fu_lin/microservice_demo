package com.gl.authorityManage.authorization.federation;

import cn.hutool.core.util.StrUtil;
import com.gl.authorityCommon.constants.SecurityConstant;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;

import java.util.*;
import java.util.stream.Collectors;

public final class FederatedTokenCustomizer implements OAuth2TokenCustomizer<JwtEncodingContext> {

    private final Set<String> ID_TOKEN_CLAIMS = Set.of(
            IdTokenClaimNames.ISS,
            IdTokenClaimNames.SUB,
            IdTokenClaimNames.AUD,
            IdTokenClaimNames.EXP,
            IdTokenClaimNames.IAT,
            IdTokenClaimNames.AUTH_TIME,
            IdTokenClaimNames.NONCE,
            IdTokenClaimNames.ACR,
            IdTokenClaimNames.AMR,
            IdTokenClaimNames.AZP,
            IdTokenClaimNames.AT_HASH,
            IdTokenClaimNames.C_HASH
    );

    private Map<String, Object> extractClaims(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        if (principal instanceof OidcUser oidcUser) {
            OidcIdToken idToken = oidcUser.getIdToken();
            return idToken.getClaims();
        }
        if (principal instanceof OAuth2User oAuth2User) {
            return oAuth2User.getAttributes();
        }
        return Collections.emptyMap();
    }

    @Override
    public void customize(JwtEncodingContext context) {
        String token = context.getTokenType().getValue();
        Authentication principal = context.getPrincipal();
        JwtClaimsSet.Builder jwtClaims = context.getClaims();

        if (StrUtil.equals(OidcParameterNames.ID_TOKEN, token)) {
            Map<String, Object> thirdPartyClaims = this.extractClaims(principal);
            jwtClaims.claims((existingClaims) -> {
                // Remove conflicting claims set by this authorization server
                Set<String> keys = existingClaims.keySet();
                keys.forEach(thirdPartyClaims::remove);
                // Remove standard id_token claims that could cause problems with clients
                ID_TOKEN_CLAIMS.forEach(thirdPartyClaims::remove);
                // Add all other claims directly to id_token
                existingClaims.putAll(thirdPartyClaims);
            });
        }

        // 检查登录用户信息与OAuth2User对象是否匹配，添加loginType属性到TOKEN中
        if (principal.getPrincipal() instanceof OAuth2User user) {
            Object loginType = user.getAttribute(SecurityConstant.LOGIN_TYPE_NAME);
            // 检验是否为String类型和是否不为空
            if (loginType instanceof String) {
                jwtClaims.claim(SecurityConstant.LOGIN_TYPE_NAME, loginType);
            }
        }

        // 检查登录用户信息与UserDetails是否匹配，排除掉没有用户参与的流程
        if (principal.getPrincipal() instanceof UserDetails user) {
            // 获取申请的scopes
            Set<String> scopes = context.getAuthorizedScopes();
            // 获取用户的权限
            Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
            /* 提取权限转化成字符串 1.判断是否为空 2.如果为空，放入初始化参数 */
            Collection<? extends GrantedAuthority> collection = Optional.ofNullable(authorities).orElse(Collections.emptyList());
            Set<String> authoritySet = collection.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
            /* 合并scope与用户信息 */
            authoritySet.addAll(scopes);
            /* 将权限信息放入JWT的claims中（也可以生成一个以指定字符分割的字符串放入，后续可以继续放入其他信息，如果角色、头像等）*/
            jwtClaims.claim(SecurityConstant.AUTHORITIES_KEY, authoritySet);
        }
    }

}
