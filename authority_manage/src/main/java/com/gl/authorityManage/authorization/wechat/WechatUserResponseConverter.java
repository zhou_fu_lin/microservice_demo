//package com.gl.authorityManage.authorization.wechat;
//
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 微信用户信息响应转换器
// */
//public class WechatUserResponseConverter extends MappingJackson2HttpMessageConverter {
//
//    public WechatUserResponseConverter() {
//        List<MediaType> mediaTypeList = new ArrayList<>(super.getSupportedMediaTypes());
//        mediaTypeList.add(MediaType.TEXT_PLAIN);
//        super.setSupportedMediaTypes(mediaTypeList);
//    }
//
//}
