//package com.gl.authorityManage.authorization.sms;
//
//import cn.hutool.core.util.StrUtil;
//import com.gl.authorityCommon.constants.ConfigConstant;
//import com.gl.authorityCommon.constants.SecurityConstant;
//import com.gl.authorityManage.utils.SecurityUtil;
//import jakarta.servlet.http.HttpServletRequest;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.oauth2.core.AuthorizationGrantType;
//import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
//import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
//import org.springframework.security.web.authentication.AuthenticationConverter;
//import org.springframework.util.MultiValueMap;
//import org.springframework.util.StringUtils;
//
//import java.util.*;
//
//public class SmsCaptchaGrantAuthenticationConverter implements AuthenticationConverter {
//
//    @Override
//    public Authentication convert(HttpServletRequest request) {
//        /* grant_type (REQUIRED) */
//        String grantType = request.getParameter(OAuth2ParameterNames.GRANT_TYPE);
//        if (!StrUtil.equals(SecurityConstant.GRANT_TYPE_SMS_CODE, grantType)) {
//            return null;
//        }
//
//        /* 这里目前是客户端认证信息 */
//        Authentication clientPrincipal = SecurityContextHolder.getContext().getAuthentication();
//
//        /* 获取请求中的参数 */
//        MultiValueMap<String, String> parameters = SecurityUtil.getParameters(request);
//
//        /* scope (OPTIONAL) */
//        String scope = parameters.getFirst(OAuth2ParameterNames.SCOPE);
//        if (StrUtil.isNotBlank(scope) && parameters.get(OAuth2ParameterNames.SCOPE).size() != 1) {
//            SecurityUtil.throwOAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST, "OAuth 2.0 Parameter: " + OAuth2ParameterNames.SCOPE, ConfigConstant.ERROR_URI_5_2);
//        }
//        Set<String> requestedScopes = null;
//        if (StrUtil.isNotBlank(scope)) {
//            List<String> scopeList = Arrays.asList(StringUtils.delimitedListToStringArray(scope, " "));
//            requestedScopes = new HashSet<>(scopeList);
//        }
//
//        /* Mobile phone number (REQUIRED) */
//        String username = parameters.getFirst(SecurityConstant.OAUTH_PARAMETER_NAME_PHONE);
//        if (StrUtil.isBlank(username) || parameters.get(SecurityConstant.OAUTH_PARAMETER_NAME_PHONE).size() != 1) {
//            SecurityUtil.throwOAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST, "OAuth 2.0 Parameter: " + SecurityConstant.OAUTH_PARAMETER_NAME_PHONE, ConfigConstant.ERROR_URI_5_2);
//        }
//
//        // SMS verification code (REQUIRED)
//        String password = parameters.getFirst(SecurityConstant.OAUTH_PARAMETER_NAME_SMS_CAPTCHA);
//        if (StrUtil.isBlank(password) || parameters.get(SecurityConstant.OAUTH_PARAMETER_NAME_SMS_CAPTCHA).size() != 1) {
//            SecurityUtil.throwOAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST, "OAuth 2.0 Parameter: " + SecurityConstant.OAUTH_PARAMETER_NAME_SMS_CAPTCHA, ConfigConstant.ERROR_URI_5_2);
//        }
//
//        // 提取附加参数
//        Map<String, Object> additionalParameters = new HashMap<>();
//        parameters.forEach((key, value) -> {
//            if (StrUtil.equals(key, OAuth2ParameterNames.GRANT_TYPE) || StrUtil.equals(key, OAuth2ParameterNames.CLIENT_ID)) {
//                return;
//            }
//            additionalParameters.put(key, value.get(0));
//        });
//
//        // 构建AbstractAuthenticationToken子类实例并返回
//        return new SmsCaptchaGrantAuthenticationToken(new AuthorizationGrantType(SecurityConstant.GRANT_TYPE_SMS_CODE), clientPrincipal, requestedScopes, additionalParameters);
//    }
//
//}
