//package com.gl.authorityManage.authorization.wechat;
//
//import cn.hutool.core.util.StrUtil;
//import com.gl.authorityCommon.constants.SecurityConstant;
//import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
//import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequestEntityConverter;
//import org.springframework.security.oauth2.client.registration.ClientRegistration;
//import org.springframework.util.MultiValueMap;
//
///**
// * 微信登录TOKEN请求处理类
// */
//public class WechatCodeGrantRequestEntityConverter extends OAuth2AuthorizationCodeGrantRequestEntityConverter {
//
//    @Override
//    protected MultiValueMap<String, String> createParameters(OAuth2AuthorizationCodeGrantRequest authorizationCodeGrantRequest) {
//        // 判断是否微信登录，并获取TOKEN的APPID与SECRET的值
//        ClientRegistration clientRegistration = authorizationCodeGrantRequest.getClientRegistration();
//        MultiValueMap<String, String> parameter = super.createParameters(authorizationCodeGrantRequest);
//
//        String registrationId = clientRegistration.getRegistrationId();
//        if (StrUtil.equals(SecurityConstant.THIRD_LOGIN_WECHAT, registrationId)) {
//            parameter.add(SecurityConstant.WECHAT_PARAMETER_APPID, clientRegistration.getClientId());
//            parameter.add(SecurityConstant.WECHAT_PARAMETER_SECRET, clientRegistration.getClientSecret());
//        }
//        return parameter;
//    }
//}
