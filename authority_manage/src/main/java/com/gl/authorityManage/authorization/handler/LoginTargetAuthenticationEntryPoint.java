package com.gl.authorityManage.authorization.handler;

import com.gl.authorityCommon.constants.ConfigConstant;
import com.gl.authorityCommon.constants.SecurityConstant;
import com.gl.authorityCommon.model.po.Result;
import com.gl.authorityCommon.utils.JsonUtil;
import com.gl.authorityCommon.utils.ResultUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.UrlUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 重定向至登录处理
 */
@Slf4j
public class LoginTargetAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

    private final String deviceActivateUri;

    public LoginTargetAuthenticationEntryPoint(String loginFormUrl, String deviceActivateUri) {
        super(loginFormUrl);
        this.deviceActivateUri = deviceActivateUri;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) throws IOException, ServletException {
        /* 兼容设备码前后端分离 */
//        if (request.getRequestURI().equals(ConfigConstant.DEVICE_VERIFICATION_URI) && UrlUtils.isAbsoluteUrl(deviceActivateUri)) {
//            /* 如果是请求验证设备激活码(user_code)时未登录并且设备码验证页面是前后端分离的那种则写回json */
//            Result<String> success = ResultUtil.error(HttpStatus.UNAUTHORIZED.value(), ("登录已失效，请重新打开设备提供的验证地址"));
//            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
//            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
//            response.getWriter().write(JsonUtil.objectCovertToJson(success));
//            response.getWriter().flush();
//            return;
//        }

        /* 获取登录表单地址 */
        String loginForm = determineUrlToUseForThisRequest(request, response, authenticationException);
        if (!UrlUtils.isAbsoluteUrl(loginForm)) {
            /* 不是绝对路径，调用父类方法处理 */
            super.commence(request, response, authenticationException);
            return;
        }

        /* 将原路径的参数，放到跳转路径上 */
        Map<String, Object> resultData = new HashMap<>();
        Map<String, String[]> params = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            String[] arrays = entry.getValue();
            Object obj = (arrays != null && arrays.length == 1) ? arrays[0] : arrays;
            resultData.put(entry.getKey(), obj);
        }
        /* 重定向地址添加nonceId参数，该参数的值为sessionId，绝对路径在重定向前添加target参数 */
//        StringBuffer requestUrl = request.getRequestURL();
//        resultData.put(SecurityConstant.TARGET_HEADER_NAME, requestUrl.toString());
//        resultData.put(SecurityConstant.LOGIN_PAGE_HEADER_NAME, ConfigConstant.LOGIN_URL);
        log.info("重定向至前后端分离的登录页面：{}", JsonUtil.objectCovertToJson(resultData));
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        Result<Map<String, Object>> success = ResultUtil.success(resultData);
        response.getWriter().write(JsonUtil.objectCovertToJson(success));
        response.getWriter().flush();
    }

}
