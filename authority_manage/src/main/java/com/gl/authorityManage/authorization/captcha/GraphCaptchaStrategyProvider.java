package com.gl.authorityManage.authorization.captcha;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gl.authorityCommon.constants.RedisConstant;
import com.gl.authorityCommon.constants.SecurityConstant;
import com.gl.authorityManage.component.RedisOperator;
import com.gl.authorityManage.exception.InvalidCaptchaException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Slf4j
public class GraphCaptchaStrategyProvider implements CaptchaStrategyProvider {

    @Override
    public Boolean additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication, RedisOperator<String> redisOperator) {
        log.info("Authenticate graph captcha, No additional authentication is required.");
        return true;
    }

    @Override
    public Authentication authenticate(Authentication authentication, RedisOperator<String> redisOperator) {

        log.info("Authenticate graph captcha...");

        // 获取当前request
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            throw new InvalidCaptchaException("Failed to get the current request.");
        }
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();

        // 获取参数中的验证码
        String code = request.getParameter(SecurityConstant.CAPTCHA_CODE_NAME);
        if (ObjectUtil.isEmpty(code)) {
            throw new InvalidCaptchaException("The captcha cannot be empty.");
        }

        // 获取获取缓存的验证码
        String captchaId = request.getParameter(SecurityConstant.CAPTCHA_ID_NAME);
        String captchaCode = redisOperator.getAndDelete(RedisConstant.GRAPH_CAPTCHA_PREFIX_KEY + captchaId);
        if (StrUtil.isBlank(captchaCode)) {
            throw new InvalidCaptchaException("The graph captcha is abnormal. Obtain it again.");
        }
        if (!captchaCode.equalsIgnoreCase(code)) {
            throw new InvalidCaptchaException("The graph captcha is incorrect.");
        }

        log.info("Graph captcha authenticated.");

        return authentication;
    }

}
