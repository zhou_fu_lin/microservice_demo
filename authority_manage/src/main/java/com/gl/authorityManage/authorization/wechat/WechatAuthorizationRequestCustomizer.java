//package com.gl.authorityManage.authorization.wechat;
//
//import cn.hutool.core.util.ObjectUtil;
//import com.gl.authorityCommon.constants.SecurityConstant;
//import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
//import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
//
//import java.util.function.Consumer;
//
///**
// * 自定义微信登录认证请求(PC端第三方登录认证时，需要严格遵守微信的参数请求顺序，不然会白屏)
// */
//public class WechatAuthorizationRequestCustomizer implements Consumer<OAuth2AuthorizationRequest.Builder> {
//
//    @Override
//    public void accept(OAuth2AuthorizationRequest.Builder builder) {
//        OAuth2AuthorizationRequest oAuth2AuthorizationRequest = builder.build();
//        Object registrationId = oAuth2AuthorizationRequest.getAttribute(OAuth2ParameterNames.REGISTRATION_ID);
//        /* 判断登录类型是否微信 */
//        if (ObjectUtil.equal(registrationId, SecurityConstant.THIRD_LOGIN_WECHAT)) {
//            builder.additionalParameters(params -> params.put(SecurityConstant.WECHAT_PARAMETER_APPID, oAuth2AuthorizationRequest.getClientId()));
//            builder.additionalParameters(params -> params.put(SecurityConstant.WECHAT_PARAMETER_FORCE_POPUP, true));
//        }
//    }
//
//}
