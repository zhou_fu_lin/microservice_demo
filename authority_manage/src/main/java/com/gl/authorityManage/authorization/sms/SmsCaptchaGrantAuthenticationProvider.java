//package com.gl.authorityManage.authorization.sms;
//
//import cn.hutool.core.lang.Assert;
//import cn.hutool.core.util.ObjectUtil;
//import com.gl.authorityCommon.constants.ConfigConstant;
//import com.gl.authorityCommon.constants.SecurityConstant;
//import com.gl.authorityManage.utils.SecurityUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.oauth2.core.*;
//import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
//import org.springframework.security.oauth2.core.oidc.OidcIdToken;
//import org.springframework.security.oauth2.core.oidc.OidcScopes;
//import org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames;
//import org.springframework.security.oauth2.jwt.Jwt;
//import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
//import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
//import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
//import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AccessTokenAuthenticationToken;
//import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientAuthenticationToken;
//import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
//import org.springframework.security.oauth2.server.authorization.context.AuthorizationServerContextHolder;
//import org.springframework.security.oauth2.server.authorization.token.DefaultOAuth2TokenContext;
//import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenContext;
//import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;
//
//import java.security.Principal;
//import java.util.HashMap;
//import java.util.LinkedHashSet;
//import java.util.Map;
//import java.util.Set;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//@Slf4j
//public class SmsCaptchaGrantAuthenticationProvider implements AuthenticationProvider {
//
//    private OAuth2TokenGenerator<?> oAuth2TokenGenerator;
//
//    private AuthenticationManager authenticationManager;
//
//    private OAuth2AuthorizationService oAuth2AuthorizationService;
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        SmsCaptchaGrantAuthenticationToken smsCaptchaGrantAuthenticationToken = (SmsCaptchaGrantAuthenticationToken) authentication;
//        /* Ensure the client authenticated */
//        OAuth2ClientAuthenticationToken oAuth2ClientAuthenticationToken = SecurityUtil.getAuthenticatedClient(smsCaptchaGrantAuthenticationToken);
//        RegisteredClient registeredClient = oAuth2ClientAuthenticationToken.getRegisteredClient();
//        /* Ensure the client is configured to use this authorization grant type */
//        if (registeredClient == null || !registeredClient.getAuthorizationGrantTypes().contains(smsCaptchaGrantAuthenticationToken.getAuthorizationGrantType())) {
//            throw new OAuth2AuthenticationException(OAuth2ErrorCodes.UNAUTHORIZED_CLIENT);
//        }
//        /* 验证scope */
//        Set<String> authorizedScopes = registeredClient.getScopes();
//        Set<String> requestedScopes = smsCaptchaGrantAuthenticationToken.getScopes();
//        if (!ObjectUtil.isEmpty(requestedScopes)) {
//            Set<String> scopes = registeredClient.getScopes();
//            Stream<String> unauthorizedScopeStream = requestedScopes.stream();
//            Set<String> unauthorizedScopes = unauthorizedScopeStream.filter(scope -> !scopes.contains(scope)).collect(Collectors.toSet());
//            if (!ObjectUtil.isEmpty(unauthorizedScopes)) {
//                SecurityUtil.throwOAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST, "OAuth 2.0 Parameter: " + OAuth2ParameterNames.SCOPE, ConfigConstant.ERROR_URI_5_2);
//            }
//            authorizedScopes = new LinkedHashSet<>(requestedScopes);
//        }
//
//        if (log.isTraceEnabled()) {
//            log.trace("Validated token request parameters");
//        }
//
//        /* 进行认证 */
//        Authentication authenticate = getAuthenticateUser(smsCaptchaGrantAuthenticationToken);
//        /* 以下内容摘抄自OAuth2AuthorizationCodeAuthenticationProvider */
//        DefaultOAuth2TokenContext.Builder defaultOAuth2TokenContextBuilder = DefaultOAuth2TokenContext.builder()
//                .registeredClient(registeredClient)
//                .principal(authenticate)
//                .authorizationServerContext(AuthorizationServerContextHolder.getContext())
//                .authorizedScopes(authorizedScopes)
//                .authorizationGrantType(smsCaptchaGrantAuthenticationToken.getAuthorizationGrantType())
//                .authorizationGrant(smsCaptchaGrantAuthenticationToken);
//        /* Initialize the OAuth2Authorization */
//        OAuth2Authorization.Builder oAuth2AuthorizationBuilder = OAuth2Authorization
//                .withRegisteredClient(registeredClient)
//                /* 加入当前用户认证信息，防止刷新token时因获取不到认证信息而抛出空指针异常，存入授权scope */
//                .authorizedScopes(authorizedScopes)
//                /* 当前授权用户名称 */
//                .principalName(authenticate.getName())
//                /* 设置当前用户名称 */
//                .attribute(Principal.class.getName(), authenticate)
//                .authorizationGrantType(smsCaptchaGrantAuthenticationToken.getAuthorizationGrantType());
//        /* Access token */
//        OAuth2TokenContext oAuth2TokenContext = defaultOAuth2TokenContextBuilder.tokenType(OAuth2TokenType.ACCESS_TOKEN).build();
//        OAuth2Token generateAccessToken = this.oAuth2TokenGenerator.generate(oAuth2TokenContext);
//        if (generateAccessToken == null) {
//            OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.SERVER_ERROR, "The token generator failed to generate the access token.", ConfigConstant.ERROR_URI_5_2);
//            throw new OAuth2AuthenticationException(error);
//        }
//
//        if (log.isTraceEnabled()) {
//            log.trace("Generated access token");
//        }
//
//        OAuth2AccessToken oAuth2AccessToken = new OAuth2AccessToken(
//                OAuth2AccessToken.TokenType.BEARER,
//                generateAccessToken.getTokenValue(),
//                generateAccessToken.getIssuedAt(),
//                generateAccessToken.getExpiresAt(),
//                oAuth2TokenContext.getAuthorizedScopes()
//        );
//        if (generateAccessToken instanceof ClaimAccessor) {
//            oAuth2AuthorizationBuilder.token(oAuth2AccessToken, (metaData) -> {
//                metaData.put(OAuth2Authorization.Token.CLAIMS_METADATA_NAME, ((ClaimAccessor) generateAccessToken).getClaims());
//            });
//        } else {
//            oAuth2AuthorizationBuilder.accessToken(oAuth2AccessToken);
//        }
//        /* Refresh token */
//        OAuth2RefreshToken oAuth2RefreshToken = null;
//        Set<AuthorizationGrantType> authorizationGrantTypeSet = registeredClient.getAuthorizationGrantTypes();
//        ClientAuthenticationMethod clientAuthenticationMethod = oAuth2ClientAuthenticationToken.getClientAuthenticationMethod();
//        /* Do not issue refresh token to public client */
//        if (authorizationGrantTypeSet.contains(AuthorizationGrantType.REFRESH_TOKEN) && !clientAuthenticationMethod.equals(ClientAuthenticationMethod.NONE)) {
//            oAuth2TokenContext = defaultOAuth2TokenContextBuilder.tokenType(OAuth2TokenType.REFRESH_TOKEN).build();
//            OAuth2Token generateRefreshToken = this.oAuth2TokenGenerator.generate(oAuth2TokenContext);
//            if (!(generateRefreshToken instanceof OAuth2RefreshToken)) {
//                OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.SERVER_ERROR, "The token generator failed to generate the refresh token.", ConfigConstant.ERROR_URI_5_2);
//                throw new OAuth2AuthenticationException(error);
//            }
//
//            if (log.isTraceEnabled()) {
//                log.trace("Generated refresh token");
//            }
//
//            oAuth2RefreshToken = (OAuth2RefreshToken) generateRefreshToken;
//            oAuth2AuthorizationBuilder.refreshToken(oAuth2RefreshToken);
//        }
//        /* ID token */
//        OidcIdToken oidcIdToken;
//        Map<String, Object> additionalParameters = new HashMap<>();
//        if (authorizedScopes.contains(OidcScopes.OPENID)) {
//            OAuth2TokenType tokenType = new OAuth2TokenType(OidcParameterNames.ID_TOKEN);
//            /* ID token customizer may need access to the access token and/or refresh token */
//            OAuth2Authorization authorization = oAuth2AuthorizationBuilder.build();
//            oAuth2TokenContext = defaultOAuth2TokenContextBuilder.tokenType(tokenType).authorization(authorization).build();
//            /* @formatter:on */
//            OAuth2Token generateIdToken = this.oAuth2TokenGenerator.generate(oAuth2TokenContext);
//            if (!(generateIdToken instanceof Jwt)) {
//                OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.SERVER_ERROR, "The token generator failed to generate the ID token.", ConfigConstant.ERROR_URI_5_2);
//                throw new OAuth2AuthenticationException(error);
//            }
//
//            if (log.isTraceEnabled()) {
//                log.trace("Generated id token");
//            }
//
//            oidcIdToken = new OidcIdToken(
//                    generateIdToken.getTokenValue(),
//                    generateIdToken.getIssuedAt(),
//                    generateIdToken.getExpiresAt(),
//                    ((Jwt) generateIdToken).getClaims()
//            );
//            oAuth2AuthorizationBuilder.token(oidcIdToken, (metaData) -> {
//                metaData.put(OAuth2Authorization.Token.CLAIMS_METADATA_NAME, oidcIdToken.getClaims());
//            });
//
//            additionalParameters.put(OidcParameterNames.ID_TOKEN, oidcIdToken.getTokenValue());
//        }
//
//        /* Save the OAuth2Authorization */
//        OAuth2Authorization oAuth2Authorization = oAuth2AuthorizationBuilder.build();
//        this.oAuth2AuthorizationService.save(oAuth2Authorization);
//
//        return new OAuth2AccessTokenAuthenticationToken(registeredClient, oAuth2ClientAuthenticationToken, oAuth2AccessToken, oAuth2RefreshToken, additionalParameters);
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return SmsCaptchaGrantAuthenticationToken.class.isAssignableFrom(authentication);
//    }
//
//    public void setOAuth2TokenGenerator(OAuth2TokenGenerator<?> oAuth2TokenGenerator) {
//        Assert.notNull(oAuth2TokenGenerator, "oAuth2TokenGenerator cannot be null");
//        this.oAuth2TokenGenerator = oAuth2TokenGenerator;
//    }
//
//    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
//        Assert.notNull(authenticationManager, "authenticationManager cannot be null");
//        this.authenticationManager = authenticationManager;
//    }
//
//    public void setOAuth2AuthorizationService(OAuth2AuthorizationService oAuth2AuthorizationService) {
//        Assert.notNull(oAuth2AuthorizationService, "oAuth2AuthorizationService cannot be null");
//        this.oAuth2AuthorizationService = oAuth2AuthorizationService;
//    }
//
//    private Authentication getAuthenticateUser(SmsCaptchaGrantAuthenticationToken smsCaptchaGrantAuthenticationToken) {
//        Map<String, Object> additionalParameters = smsCaptchaGrantAuthenticationToken.getAdditionalParameters();
//        String phone = (String) additionalParameters.get(SecurityConstant.OAUTH_PARAMETER_NAME_PHONE);
//        String smsCaptcha = (String) additionalParameters.get(SecurityConstant.OAUTH_PARAMETER_NAME_SMS_CAPTCHA);
//        /**
//         * 构建UsernamePasswordAuthenticationToken通过AbstractUserDetailsAuthenticationProvider及其子类对手机号与验证码进行校验
//         * 短信验证与密码模式区别不大，如果是短信验证模式则在SmsCaptchaLoginAuthenticationProvider中加一个校验
//         * 使框架支持手机号、验证码校验，反之不加就是账号密码登录
//         */
//        Authentication authenticate = null;
//        UsernamePasswordAuthenticationToken unauthenticated = UsernamePasswordAuthenticationToken.unauthenticated(phone, smsCaptcha);
//        try {
//            authenticate = authenticationManager.authenticate(unauthenticated);
//        } catch (Exception e) {
//            SecurityUtil.throwOAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST, "认证失败：手机号或验证码错误.", ConfigConstant.ERROR_URI_5_2);
//        }
//        return authenticate;
//    }
//}
