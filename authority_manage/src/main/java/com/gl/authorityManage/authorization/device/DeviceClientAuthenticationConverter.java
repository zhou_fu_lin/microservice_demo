//package com.gl.authorityManage.authorization.device;
//
//import cn.hutool.core.util.StrUtil;
//import jakarta.servlet.http.HttpServletRequest;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.oauth2.core.AuthorizationGrantType;
//import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
//import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
//import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
//import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
//import org.springframework.security.web.authentication.AuthenticationConverter;
//import org.springframework.security.web.util.matcher.AndRequestMatcher;
//import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//import org.springframework.security.web.util.matcher.RequestMatcher;
//
///**
// * 获取请求参数转化为DeviceClientAuthenticationToken对象
// */
//public class DeviceClientAuthenticationConverter implements AuthenticationConverter {
//
//    private final RequestMatcher deviceAuthorizationRequestMatcher;
//
//    private final RequestMatcher deviceAccessTokenRequestMatcher;
//
//    public DeviceClientAuthenticationConverter(String deviceAuthorizationEndpointUri) {
//        this.deviceAuthorizationRequestMatcher = new AndRequestMatcher(
//                new AntPathRequestMatcher(deviceAuthorizationEndpointUri, HttpMethod.POST.name()),
//                (request) -> {
//                    String clientId = request.getParameter(OAuth2ParameterNames.CLIENT_ID);
//                    return StrUtil.isNotBlank(clientId);
//                }
//        );
//        this.deviceAccessTokenRequestMatcher = (request) -> {
//            String deviceCode = AuthorizationGrantType.DEVICE_CODE.getValue();
//            String clientId = request.getParameter(OAuth2ParameterNames.CLIENT_ID);
//            String grantType = request.getParameter(OAuth2ParameterNames.GRANT_TYPE);
//            String deviceCodeValue = request.getParameter(OAuth2ParameterNames.DEVICE_CODE);
//
//            boolean flag = StrUtil.isNotBlank(clientId) && StrUtil.isNotBlank(deviceCodeValue);
//            return StrUtil.equals(deviceCode, grantType) && flag;
//        };
//    }
//
//    @Override
//    public Authentication convert(HttpServletRequest request) {
//        if (!this.deviceAuthorizationRequestMatcher.matches(request) && !this.deviceAccessTokenRequestMatcher.matches(request)) {
//            return null;
//        }
//        String clientId = request.getParameter(OAuth2ParameterNames.CLIENT_ID);
//        String[] clientIdValue = request.getParameterValues(OAuth2ParameterNames.CLIENT_ID);
//        if (StrUtil.isBlank(clientId) || clientIdValue.length != 1) {
//            throw new OAuth2AuthenticationException(OAuth2ErrorCodes.INVALID_REQUEST);
//        }
//        return new DeviceClientAuthenticationToken(clientId, ClientAuthenticationMethod.NONE, null, null);
//    }
//}
