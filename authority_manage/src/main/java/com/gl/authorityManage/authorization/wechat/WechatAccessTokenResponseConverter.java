//package com.gl.authorityManage.authorization.wechat;
//
//import cn.hutool.core.util.StrUtil;
//import org.springframework.core.convert.converter.Converter;
//import org.springframework.security.oauth2.core.OAuth2AccessToken;
//import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
//import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
//import org.springframework.util.StringUtils;
//
//import java.util.*;
//
///**
// * 微信登录TOKEN响应处理类
// */
//public class WechatAccessTokenResponseConverter implements Converter<Map<String, Object>, OAuth2AccessTokenResponse> {
//
//    private final Set<String> TOKEN_RESPONSE_PARAMETER_NAMES = Set.of(
//            OAuth2ParameterNames.REFRESH_TOKEN,
//            OAuth2ParameterNames.ACCESS_TOKEN,
//            OAuth2ParameterNames.EXPIRES_IN,
//            OAuth2ParameterNames.TOKEN_TYPE,
//            OAuth2ParameterNames.SCOPE
//    );
//
//    @Override
//    public OAuth2AccessTokenResponse convert(Map<String, Object> source) {
//        long expiresIn = getParameterValue(source, OAuth2ParameterNames.EXPIRES_IN, 0L);
//        String refreshToken = getParameterValue(source, OAuth2ParameterNames.REFRESH_TOKEN);
//        String accessToken = getParameterValue(source, OAuth2ParameterNames.ACCESS_TOKEN);
//        OAuth2AccessToken.TokenType tokenType = OAuth2AccessToken.TokenType.BEARER;
//        Set<String> scopes = getScopes(source);
//
//        Map<String, Object> parameterMap = new LinkedHashMap<>();
//        for (Map.Entry<String, Object> entry : source.entrySet()) {
//            if (TOKEN_RESPONSE_PARAMETER_NAMES.contains(entry.getKey())) {
//                continue;
//            }
//            parameterMap.put(entry.getKey(), entry.getValue());
//        }
//        return OAuth2AccessTokenResponse.withToken(accessToken)
//                .additionalParameters(parameterMap)
//                .refreshToken(refreshToken)
//                .tokenType(tokenType)
//                .expiresIn(expiresIn)
//                .scopes(scopes)
//                .build();
//    }
//
//    private Set<String> getScopes(Map<String, Object> source) {
//        if (source.containsKey(OAuth2ParameterNames.SCOPE)) {
//            String scope = getParameterValue(source, OAuth2ParameterNames.SCOPE);
//            String[] arrays = StringUtils.delimitedListToStringArray(scope, " ");
//            return new HashSet<>(Arrays.asList(arrays));
//        }
//        return Collections.emptySet();
//    }
//
//    private String getParameterValue(Map<String, Object> source, String key) {
//        Object obj = source.get(key);
//        return StrUtil.toStringOrNull(obj);
//    }
//
//    private long getParameterValue(Map<String, Object> source, String key, long defaultValue) {
//        Object obj = source.get(key);
//        if (obj == null) {
//            return defaultValue;
//        }
//        if (obj instanceof Long) {
//            return (Long) obj;
//        }
//        if (obj instanceof  Integer) {
//            return (Integer) obj;
//        }
//        try {
//            String str = obj.toString();
//            return Long.parseLong(str);
//        } catch (NumberFormatException e) {
//            return defaultValue;
//        }
//    }
//}
