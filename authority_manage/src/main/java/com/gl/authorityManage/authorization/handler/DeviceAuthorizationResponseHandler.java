//package com.gl.authorityManage.authorization.handler;
//
//import com.gl.authorityCommon.model.po.Result;
//import com.gl.authorityCommon.utils.JsonUtil;
//import com.gl.authorityCommon.utils.ResultUtil;
//import jakarta.servlet.ServletException;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.http.MediaType;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//
//public class DeviceAuthorizationResponseHandler implements AuthenticationSuccessHandler {
//
//    /**
//     * 设备码验证成功后跳转地址
//     */
//    private final String deviceActivatedUri;
//
//    public DeviceAuthorizationResponseHandler(String deviceActivatedUri) {
//        this.deviceActivatedUri = deviceActivatedUri;
//    }
//
//    @Override
//    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
//        // 写回json数据
//        Result<Object> result = ResultUtil.success(deviceActivatedUri);
//        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
//        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
//        response.getWriter().write(JsonUtil.objectCovertToJson(result));
//        response.getWriter().flush();
//    }
//
//}
