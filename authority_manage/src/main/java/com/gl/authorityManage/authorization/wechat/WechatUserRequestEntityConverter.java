//package com.gl.authorityManage.authorization.wechat;
//
//import cn.hutool.core.util.StrUtil;
//import com.gl.authorityCommon.constants.SecurityConstant;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.RequestEntity;
//import org.springframework.security.oauth2.client.registration.ClientRegistration;
//import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
//import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequestEntityConverter;
//import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
//import org.springframework.web.util.UriComponentsBuilder;
//
//import java.net.URI;
//
///**
// * 微信登录获取用户信息请求转换器
// */
//public class WechatUserRequestEntityConverter extends OAuth2UserRequestEntityConverter {
//
//    @Override
//    public RequestEntity<?> convert(OAuth2UserRequest userRequest) {
//        // 获取配置文件的客户端信息
//        ClientRegistration clientRegistration = userRequest.getClientRegistration();
//        String registrationId = clientRegistration.getRegistrationId();
//        if (StrUtil.equals(SecurityConstant.THIRD_LOGIN_WECHAT, registrationId)) {
//            Object openId = userRequest.getAdditionalParameters().get(SecurityConstant.WECHAT_PARAMETER_OPENID);
//            String formUri = clientRegistration.getProviderDetails().getUserInfoEndpoint().getUri();
//            String accessToken = userRequest.getAccessToken().getTokenValue();
//
//            URI uri = UriComponentsBuilder
//                    .fromUriString(formUri)
//                    .queryParam(SecurityConstant.WECHAT_PARAMETER_OPENID, openId)
//                    .queryParam(OAuth2ParameterNames.ACCESS_TOKEN, accessToken)
//                    .build()
//                    .toUri();
//            return new RequestEntity<>(HttpMethod.GET, uri);
//        }
//        return super.convert(userRequest);
//    }
//
//
//}
