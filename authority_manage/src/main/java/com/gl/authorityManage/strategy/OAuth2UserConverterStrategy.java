package com.gl.authorityManage.strategy;

import com.gl.authorityManage.model.OAuth2ThirdAccount;
import org.springframework.security.oauth2.core.user.OAuth2User;

/**
 * oauth2 第三方登录获取用户信息转换策略接口
 */
public interface OAuth2UserConverterStrategy {

    /**
     * 将oauth2登录认证信息转为账号信息
     */
    OAuth2ThirdAccount convert(OAuth2User oAuth2User);

}
