package com.gl.authorityManage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gl.authorityManage.model.SysUser;
import org.springframework.stereotype.Repository;

@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {
}
