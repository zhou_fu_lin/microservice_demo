package com.gl.authorityManage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gl.authorityManage.model.SysRole;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
