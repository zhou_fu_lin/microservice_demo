package com.gl.authorityManage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gl.authorityManage.model.SysRoleAuthority;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleAuthorityMapper extends BaseMapper<SysRoleAuthority> {
}
