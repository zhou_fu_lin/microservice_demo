package com.gl.authorityManage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gl.authorityManage.model.OAuth2ThirdAccount;
import org.springframework.stereotype.Repository;

@Repository
public interface OAuth2ThirdAccountMapper extends BaseMapper<OAuth2ThirdAccount> {
}
