package com.gl.authorityManage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gl.authorityManage.model.SysAuthority;
import org.springframework.stereotype.Repository;

@Repository
public interface SysAuthorityMapper extends BaseMapper<SysAuthority> {
}
