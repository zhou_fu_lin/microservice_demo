package com.gl.authorityManage.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.gl.authorityCommon.constants.RedisConstant;
import com.gl.authorityCommon.model.po.Result;
import com.gl.authorityCommon.utils.ResultUtil;
import com.gl.authorityManage.component.RedisOperator;
import com.gl.authorityManage.model.dto.CaptchaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    private RedisOperator<String> redisOperator;

    @GetMapping("/common/getCaptcha")
    public Result<CaptchaDto> getCaptcha(Integer width, Integer height) {
        /* 使用HUTOOL-CAPTCHA生成图形验证码并定义图形验证吗的长、宽、验证码字符数、干扰线宽度 */
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(width, height, 4,2);
        /* 生成唯一ID */
        long id = IdWorker.getId();
        String key = RedisConstant.GRAPH_CAPTCHA_PREFIX_KEY + id;
        /* 存入缓存中, 5分钟后过期 */
        CaptchaDto data = new CaptchaDto(String.valueOf(id), captcha.getCode(), captcha.getImageBase64Data());
        redisOperator.set(key, captcha.getCode(), RedisConstant.CAPTCHA_TIMEOUT_SECONDS);
        return ResultUtil.success("获取验证码.", data);
    }

    @GetMapping("/getSmsCaptcha")
    public Result<String> getSmsCaptcha(String phone) {
        /* 短信验证固定为1234 */
        String smsCaptcha = "1234";
        /* 存入缓存中, 5分钟后过期 */
        String key = RedisConstant.SMS_CAPTCHA_PREFIX_KEY + phone;
        redisOperator.set(key, smsCaptcha, RedisConstant.CAPTCHA_TIMEOUT_SECONDS);
        return ResultUtil.success("获取短信验证码成功.", smsCaptcha);
    }

}
