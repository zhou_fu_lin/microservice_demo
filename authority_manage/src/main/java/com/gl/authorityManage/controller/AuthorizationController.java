package com.gl.authorityManage.controller;

import cn.hutool.core.util.StrUtil;
import com.gl.authorityCommon.model.po.Result;
import com.gl.authorityCommon.utils.ResultUtil;
import com.gl.authorityManage.model.vo.ScopeWithDescriptionVo;
import com.gl.authorityManage.properties.CustomSecurityProperties;
import com.gl.authorityManage.service.OAuth2ThirdAccountService;
import com.gl.authorityManage.service.SysAuthorityService;
import com.gl.authorityManage.service.SysUserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsent;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Controller
public class AuthorizationController {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysAuthorityService sysAuthorityService;

    @Autowired
    private OAuth2ThirdAccountService oAuth2ThirdAccountService;

    @Autowired
    private RegisteredClientRepository registeredClientRepository;

    @Autowired
    private OAuth2AuthorizationConsentService oAuth2AuthorizationConsentService;

    @Autowired
    private CustomSecurityProperties customSecurityProperties;

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @GetMapping("/activate")
    public String activate(@RequestParam(value = "user_code", required = false) String userCode) {
        if (userCode != null) {
            return "redirect:/oauth2/device_verification?user_code=" + userCode;
        }
        return "device-activate";
    }
    
    @GetMapping("/activate/redirect")
    public String activateRedirect(@RequestParam(value = "user_code", required = false) String userCode) {

        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(customSecurityProperties.getDeviceActivateUri())
                .queryParam("userCode", userCode);
        return "redirect:" + uriBuilder.build(Boolean.TRUE).toUriString();
    }

    @GetMapping("/activated")
    public String activated() {
        return "device-activated";
    }

    @GetMapping(value = "/")
    public String success() {
        return "device-activated";
    }

    @GetMapping("/login")
    public String login(Model model, HttpSession session) {
        Object attribute = session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if (attribute instanceof AuthenticationException exception) {
            model.addAttribute("error", exception.getMessage());
        }
        return "login";
    }

    @GetMapping(value = "/oauth2/consent")
    public String consent(
            Principal principal,
            Model model,
            @RequestParam(OAuth2ParameterNames.CLIENT_ID) String clientId,
            @RequestParam(OAuth2ParameterNames.SCOPE) String scope,
            @RequestParam(OAuth2ParameterNames.STATE) String state,
            @RequestParam(name = OAuth2ParameterNames.USER_CODE, required = false) String userCode
    ) {

        // 获取consent页面所需的参数
        Map<String, Object> consentParameters = getConsentParameters(scope, state, clientId, userCode, principal);
        // 转至model中，让框架渲染页面
        consentParameters.forEach(model::addAttribute);
        return "consent";
    }


    @ResponseBody
    @GetMapping(value = "/oauth2/consent/redirect")
    public Result<Map<String, Object>> consentRedirect(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(OAuth2ParameterNames.SCOPE) String scope,
            @RequestParam(OAuth2ParameterNames.STATE) String state,
            @RequestParam(OAuth2ParameterNames.CLIENT_ID) String clientId,
            @RequestParam(name = OAuth2ParameterNames.USER_CODE, required = false) String userCode
    ) throws IOException {
        if (!UrlUtils.isAbsoluteUrl(customSecurityProperties.getConsentPageUri())) {
            // 携带当前请求参数与nonceId重定向至前端页面
            UriComponentsBuilder uriBuilder = UriComponentsBuilder
                    .fromUriString(customSecurityProperties.getConsentPageUri())
                    .queryParam(OAuth2ParameterNames.CLIENT_ID, clientId)
                    .queryParam(OAuth2ParameterNames.USER_CODE, userCode)
                    .queryParam(OAuth2ParameterNames.SCOPE, UriUtils.encode(scope, StandardCharsets.UTF_8))
                    .queryParam(OAuth2ParameterNames.STATE, UriUtils.encode(state, StandardCharsets.UTF_8));

            String uriString = uriBuilder.build(Boolean.TRUE).toUriString();
            // 不是设备码模式或码验证页面不是前后端分离的，无需返回json，直接重定向
            redirectStrategy.sendRedirect(request, response, uriString);
            return null;
        }
        // 兼容设备码，需响应JSON，由前端进行跳转
        Map<String, Object> data = new HashMap<>();
        data.put(OAuth2ParameterNames.SCOPE, scope);
        data.put(OAuth2ParameterNames.STATE, state);
        data.put(OAuth2ParameterNames.CLIENT_ID, clientId);
        data.put(OAuth2ParameterNames.USER_CODE, userCode);
        return ResultUtil.success(data);
    }

    @ResponseBody
    @GetMapping(value = "/oauth2/consent/parameter")
    public Result<Map<String, Object>> consentParameter(
            Principal principal,
            @RequestParam(OAuth2ParameterNames.CLIENT_ID) String clientId,
            @RequestParam(OAuth2ParameterNames.SCOPE) String scope,
            @RequestParam(OAuth2ParameterNames.STATE) String state,
            @RequestParam(name = OAuth2ParameterNames.USER_CODE, required = false) String userCode
    ) {
        // 获取consent页面所需的参数
        Map<String, Object> consentParameters = getConsentParameters(scope, state, clientId, userCode, principal);
        return ResultUtil.success(consentParameters);
    }

//    @ResponseBody
//    @GetMapping("/user")
//    public OAuth2UserInfoDto user(Principal principal) {
//        OAuth2UserInfoDto userInfoDto = new OAuth2UserInfoDto();
//        if (!(principal instanceof JwtAuthenticationToken jwtAuthenticationToken)) {
//            return userInfoDto;
//        }
//        /* 获取jwt解析内容 */
//        Jwt token = jwtAuthenticationToken.getToken();
//        /* 获取当前用户账号 */
//        String userName = token.getClaim(JwtClaimNames.SUB);
//        /* 获取scope */
//        List<String> scopes = token.getClaimAsStringList("scope");
//        List<String> claims = token.getClaimAsStringList(SecurityConstant.AUTHORITIES_KEY);
//        scopes = ObjectUtil.isNotEmpty(claims) ? null : scopes;
//
//        /* 后期需要把业务逻辑移至SERVICE层 */
//        LambdaQueryWrapper<SysUser> userWrapper = Wrappers.lambdaQuery(SysUser.class).eq(SysUser::getUsername, userName);
//        SysUser sysUser = sysUserService.getOne(userWrapper);
//
//        if (sysUser != null) {
//            // 填充用户的权限信息
//            fillUserAuthority(claims, sysUser, scopes);
//            BeanUtils.copyProperties(sysUser, userInfoDto);
//            // 根据用户信息查询三方登录信息
//            LambdaQueryWrapper<OAuth2ThirdAccount> accountWrapper = Wrappers.lambdaQuery(OAuth2ThirdAccount.class).eq(OAuth2ThirdAccount::getUserId, sysUser.getId());
//            OAuth2ThirdAccount oAuth2ThirdAccount = oAuth2ThirdAccountService.getOne(accountWrapper);
//            if (oAuth2ThirdAccount == null) {
//                return userInfoDto;
//            }
//            userInfoDto.setCredentialsExpiresAt(oAuth2ThirdAccount.getCredentialsExpiresAt());
//            userInfoDto.setThirdUsername(oAuth2ThirdAccount.getThirdUsername());
//            userInfoDto.setCredentials(oAuth2ThirdAccount.getCredentials());
//            return userInfoDto;
//        }
//        // 根据当前sub去三方登录表去查
//        String type = token.getClaim(SecurityConstant.LOGIN_TYPE_NAME);
//        LambdaQueryWrapper<OAuth2ThirdAccount> accountWrapper = Wrappers.lambdaQuery(OAuth2ThirdAccount.class).eq(OAuth2ThirdAccount::getUniqueId, userName).eq(OAuth2ThirdAccount::getType, type);
//        OAuth2ThirdAccount oAuth2ThirdAccount = oAuth2ThirdAccountService.getOne(accountWrapper);
//        if (oAuth2ThirdAccount == null) {
//            return userInfoDto;
//        }
//        // 再查询基础用户表
//        sysUser = sysUserService.getById(oAuth2ThirdAccount.getUserId());
//        // 填充用户的权限信息
//        fillUserAuthority(claims, sysUser, scopes);
//        // 复制基础用户信息
//        BeanUtils.copyProperties(sysUser, userInfoDto);
//        // 设置三方用户信息
//        userInfoDto.setLocation(oAuth2ThirdAccount.getLocation());
//        userInfoDto.setCredentials(oAuth2ThirdAccount.getCredentials());
//        userInfoDto.setThirdUsername(oAuth2ThirdAccount.getThirdUsername());
//        userInfoDto.setCredentialsExpiresAt(oAuth2ThirdAccount.getCredentialsExpiresAt());
//        return userInfoDto;
//    }
//
//    private void fillUserAuthority(List<String> claims, SysUser sysUser, List<String> scopes) {
//        if (!ObjectUtil.isEmpty(claims)) {
//            /* 否则设置为token中获取的 */
//            Set<CustomGrantedAuthority> authorities = claims.stream().map(CustomGrantedAuthority::new).collect(Collectors.toSet());
//            sysUser.setAuthorities(authorities);
//            return;
//        }
//        /* 如果获取不到权限信息去数据库查 */
//        List<SysAuthority> sysAuthorities = sysAuthorityService.getByUserId(sysUser.getId());
//        Set<CustomGrantedAuthority> authorities = sysAuthorities.stream().map(SysAuthority::getAuthority).map(CustomGrantedAuthority::new).collect(Collectors.toSet());
//        if (!ObjectUtil.isEmpty(scopes)) {
//            scopes.stream().map(CustomGrantedAuthority::new).forEach(authorities::add);
//        }
//        sysUser.setAuthorities(authorities);
//    }
//
//    @GetMapping(value = "/oauth2/consent")
//    public String consent(
//            Model model,
//            Principal principal,
//            @RequestParam(OAuth2ParameterNames.CLIENT_ID) String clientId,
//            @RequestParam(OAuth2ParameterNames.SCOPE) String scope,
//            @RequestParam(OAuth2ParameterNames.STATE) String state,
//            @RequestParam(name = OAuth2ParameterNames.USER_CODE, required = false) String userCode
//    ) {
//
//        // Remove scopes that were already approved
//        Set<String> scopesToApprove = new HashSet<>();
//        Set<String> previouslyApprovedScopes = new HashSet<>();
//        RegisteredClient registeredClient = this.registeredClientRepository.findByClientId(clientId);
//        if (registeredClient == null) {
//            throw new RuntimeException("客户端不存在");
//        }
//
//        OAuth2AuthorizationConsent currentAuthorizationConsent = this.oAuth2AuthorizationConsentService.findById(registeredClient.getId(), principal.getName());
//
//        Set<String> authorizedScopes;
//        if (currentAuthorizationConsent != null) {
//            authorizedScopes = currentAuthorizationConsent.getScopes();
//        } else {
//            authorizedScopes = Collections.emptySet();
//        }
//        for (String requestedScope : StringUtils.delimitedListToStringArray(scope, " ")) {
//            if (OidcScopes.OPENID.equals(requestedScope)) {
//                continue;
//            }
//            if (authorizedScopes.contains(requestedScope)) {
//                previouslyApprovedScopes.add(requestedScope);
//            } else {
//                scopesToApprove.add(requestedScope);
//            }
//        }
//
//        model.addAttribute("clientId", clientId);
//        model.addAttribute("state", state);
//        model.addAttribute("scopes", withDescription(scopesToApprove));
//        model.addAttribute("previouslyApprovedScopes", withDescription(previouslyApprovedScopes));
//        model.addAttribute("principalName", principal.getName());
//        model.addAttribute("userCode", userCode);
//        if (StringUtils.hasText(userCode)) {
//            model.addAttribute("requestURI", "/oauth2/device_verification");
//        } else {
//            model.addAttribute("requestURI", "/oauth2/authorize");
//        }
//
//        return "consent";
//    }

    private Set<ScopeWithDescriptionVo> withDescription(Set<String> scopes) {
        String DEFAULT_DESCRIPTION = "UNKNOWN SCOPE - We cannot provide information about this permission, use caution when granting this.";
        Set<ScopeWithDescriptionVo> scopeWithDescriptions = new HashSet<>();
        Map<String, String> scopeDescription = new ConcurrentHashMap<String, String>() {{
            put(OidcScopes.OPENID, "This application will be able to read your open id.");
            put(OidcScopes.PROFILE, "This application will be able to read your profile information.");
            put("message.read", "This application will be able to read your message.");
            put("message.write", "This application will be able to add new messages. It will also be able to edit and delete existing messages.");
            put("other.scope", "This is another scope example of a scope description.");
        }};
        for (String scope : scopes) {
            String description = scopeDescription.getOrDefault(scope, DEFAULT_DESCRIPTION);
            ScopeWithDescriptionVo vo = new ScopeWithDescriptionVo(scope, description);
            scopeWithDescriptions.add(vo);
        }
        return scopeWithDescriptions;
    }

    /**
     * 根据授权确认相关参数获取授权确认与未确认的scope相关参数
     *
     * @param scope     scope权限
     * @param state     state
     * @param clientId  客户端id
     * @param userCode  设备码授权流程中的用户码
     * @param principal 当前认证信息
     * @return 页面所需数据
     */
    private Map<String, Object> getConsentParameters(
            String scope,
            String state,
            String clientId,
            String userCode,
            Principal principal
    ) {

        if (principal == null) {
            throw new RuntimeException("认证信息已失效.");
        }

        // Remove scopes that were already approved
        Set<String> scopesToApprove = new HashSet<>();
        Set<String> previouslyApprovedScopes = new HashSet<>();
        RegisteredClient registeredClient = this.registeredClientRepository.findByClientId(clientId);
        if (registeredClient == null) {
            throw new RuntimeException("客户端不存在");
        }
        OAuth2AuthorizationConsent currentAuthorizationConsent = this.oAuth2AuthorizationConsentService.findById(registeredClient.getId(), principal.getName());
        Set<String> authorizedScopes = (currentAuthorizationConsent != null) ? currentAuthorizationConsent.getScopes() : Collections.emptySet();
        for (String requestedScope : StringUtils.delimitedListToStringArray(scope, " ")) {
            if (OidcScopes.OPENID.equals(requestedScope)) {
                continue;
            }
            if (authorizedScopes.contains(requestedScope)) {
                previouslyApprovedScopes.add(requestedScope);
            } else {
                scopesToApprove.add(requestedScope);
            }
        }

        Map<String, Object> parameters = new HashMap<>(7);
        parameters.put("clientId", registeredClient.getClientId());
        parameters.put("clientName", registeredClient.getClientName());
        parameters.put("state", state);
        parameters.put("scopes", withDescription(scopesToApprove));
        parameters.put("previouslyApprovedScopes", withDescription(previouslyApprovedScopes));
        parameters.put("principalName", principal.getName());
        parameters.put("userCode", userCode);

        String requestUri = StrUtil.isNotBlank(userCode) ? "/oauth2/device_verification" : "/oauth2/authorize";
        parameters.put("requestURI", requestUri);
        return parameters;
    }

}
