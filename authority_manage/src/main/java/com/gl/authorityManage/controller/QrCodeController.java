//package com.gl.authorityManage.controller;
//
//import com.gl.authorityCommon.model.po.Result;
//import com.gl.authorityCommon.utils.ResultUtil;
//import com.gl.authorityManage.model.ao.QrCodeConsentAo;
//import com.gl.authorityManage.model.ao.QrCodeScanAo;
//import com.gl.authorityManage.model.po.QrCodeFetchPo;
//import com.gl.authorityManage.model.po.QrCodeGeneratePo;
//import com.gl.authorityManage.model.po.QrCodeScanPo;
//import com.gl.authorityManage.service.QrCodeService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("qrCode")
//public class QrCodeController {
//
//    @Autowired
//    private QrCodeService qrCodeService;
//
//    @GetMapping("/login/generateQrCode")
//    public Result<QrCodeGeneratePo> generateQrCode() {
//        // 生成二维码
//        return ResultUtil.success(qrCodeService.generateQrCode());
//    }
//
//    @GetMapping("/login/fetch/{qrCodeId}")
//    public Result<QrCodeFetchPo> fetch(@PathVariable String qrCodeId) {
//        // 轮询二维码状态
//        return ResultUtil.success(qrCodeService.fetch(qrCodeId));
//    }
//
//
//    @PostMapping("/scan")
//    public Result<QrCodeScanPo> scan(@RequestBody QrCodeScanAo scanInfo) {
//        // app 扫码二维码
//        return ResultUtil.success(qrCodeService.scan(scanInfo));
//    }
//
//    @PostMapping("/consent")
//    public Result<String> consent(@RequestBody QrCodeConsentAo consentInfo) {
//        // app 确认登录
//        qrCodeService.consent(consentInfo);
//        return ResultUtil.success();
//    }
//
//}
