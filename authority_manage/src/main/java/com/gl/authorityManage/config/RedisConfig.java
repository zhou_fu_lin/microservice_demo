package com.gl.authorityManage.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisKeyValueAdapter;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.jackson2.CoreJackson2Module;

@Configuration
@EnableRedisRepositories(enableKeyspaceEvents = RedisKeyValueAdapter.EnableKeyspaceEvents.ON_STARTUP)
public class RedisConfig {

    @Autowired
    private Jackson2ObjectMapperBuilder builder;

    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        /* 字符串序列化器 */
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        /* 创建ObjectMapper并添加默认配置 */
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        /* 序列化所有字段 */
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        /* 此项必须配置，否则如果序列化的对象里边还有对象，会报如下错误：java.lang.ClassCastException: java.util.LinkedHashMap cannot be cast to XXX */
        objectMapper.activateDefaultTyping(objectMapper.getPolymorphicTypeValidator(), ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        /* 添加Security提供的Jackson Mixin */
        objectMapper.registerModule(new CoreJackson2Module());
        
        /* 存入redis时序列化值的序列化器 */
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        Jackson2JsonRedisSerializer<Object> valueSerializer = new Jackson2JsonRedisSerializer<>(objectMapper, Object.class);
        /* 设置值序列化 */
        redisTemplate.setValueSerializer(valueSerializer);
        /* 设置hash格式数据值的序列化器 */
        redisTemplate.setHashValueSerializer(valueSerializer);
        /* 默认的Key序列化器为：JdkSerializationRedisSerializer */
        redisTemplate.setKeySerializer(stringRedisSerializer);
        /* 设置字符串序列化器 */
        redisTemplate.setStringSerializer(stringRedisSerializer);
        /* 设置hash结构的key的序列化器 */
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        /* 设置连接工厂 */
        redisTemplate.setConnectionFactory(connectionFactory);
        return redisTemplate;
    }

}
