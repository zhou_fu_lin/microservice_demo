//package com.gl.authorityManage.config;
//
//import com.gl.authorityManage.authorization.handler.LoginFailureHandler;
//import com.gl.authorityManage.authorization.handler.LoginSuccessHandler;
//import com.gl.authorityManage.authorization.wechat.WechatAccessTokenResponseConverter;
//import com.gl.authorityManage.authorization.wechat.WechatAuthorizationRequestCustomizer;
//import com.gl.authorityManage.authorization.wechat.WechatCodeGrantRequestEntityConverter;
//import com.gl.authorityManage.constants.ConfigConstant;
//import com.gl.authorityManage.utils.security.SecurityUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.FormHttpMessageConverter;
//import org.springframework.security.access.annotation.Secured;
//import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
//import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
//import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
//import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
//import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
//import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver;
//import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
//import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
//import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.util.UrlUtils;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.filter.CorsFilter;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
///**
// * 资源服务器配置
// * <p>
// * {@link EnableMethodSecurity} 开启全局方法认证，启用JSR250注解支持，启用注解 {@link Secured} 支持，
// *   在Spring Security 6.0版本中将@Configuration注解从@EnableWebSecurity, @EnableMethodSecurity, @EnableGlobalMethodSecurity 和 @EnableGlobalAuthentication 中移除，使用这些注解需手动添加 @Configuration 注解
// * {@link EnableWebSecurity} 注解有两个作用:
// *   1. 加载了WebSecurityConfiguration配置类, 配置安全认证策略。
// *   2. 加载了AuthenticationConfiguration, 配置了认证信息。
// */
//@Configuration
//@EnableWebSecurity
//@EnableMethodSecurity(jsr250Enabled = true, securedEnabled = true)
//public class ResourceConfig {
//
//    @Autowired
//    private CorsFilter corsFilter;
//
//    @Autowired
//    private CustomSecurityProperties customSecurityProperties;
//
//    /**
//     * 配置认证相关的过滤器链
//     */
//    @Bean
//    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http, ClientRegistrationRepository clientRegistrationRepository) throws Exception {
//        // 添加基础的认证配置
//        SecurityUtil.applyBasicSecurity(http, corsFilter, customSecurityProperties);
//
//        http
//            /* 放行静态资源及特定资源 */
//            .authorizeHttpRequests((authorizeHttpRequestsCustomizer) -> {
//                String[] matchers = customSecurityProperties.getIgnoreUriList().toArray(new String[0]);
//                authorizeHttpRequestsCustomizer.requestMatchers(matchers).permitAll();
//                authorizeHttpRequestsCustomizer.anyRequest().authenticated();
//            })
//            /* 表单登录处理授权服务器过滤器链的重定向 */
//            .formLogin((formLoginCustomizer) -> {
//                /* 登录成功和失败改为写回json，不再重定向；兼容前后端不分离方式 */
//                formLoginCustomizer.loginPage(ConfigConstant.LOGIN_URL);
//                if (UrlUtils.isAbsoluteUrl(ConfigConstant.LOGiN_FORM_URL)) {
//                    formLoginCustomizer.successHandler(new LoginSuccessHandler());
//                    formLoginCustomizer.failureHandler(new LoginFailureHandler());
//                }
//            });
//        /* 联合登录配置 */
//        http.oauth2Login((oauth2LoginCustomizer) -> {
//            oauth2LoginCustomizer.loginPage(ConfigConstant.LOGiN_FORM_URL);
//            oauth2LoginCustomizer.authorizationEndpoint(
//                    authorizationEndpointCustomizer -> authorizationEndpointCustomizer.authorizationRequestResolver(this.authorizationRequestResolver(clientRegistrationRepository))
//            );
//            oauth2LoginCustomizer.tokenEndpoint(
//                    tokenEndpointCustomizer -> tokenEndpointCustomizer.accessTokenResponseClient(this.accessTokenResponseClient())
//            );
//        });
//        return http.build();
//    }
//
//    /**
//     * AuthorizationRequest 自定义配置
//     */
//    private OAuth2AuthorizationRequestResolver authorizationRequestResolver(ClientRegistrationRepository clientRegistrationRepository) {
//        DefaultOAuth2AuthorizationRequestResolver defaultOAuth2AuthorizationRequestResolver = new DefaultOAuth2AuthorizationRequestResolver(clientRegistrationRepository, OAuth2AuthorizationRequestRedirectFilter.DEFAULT_AUTHORIZATION_REQUEST_BASE_URI);
//        // 兼容微信登录授权申请
//        defaultOAuth2AuthorizationRequestResolver.setAuthorizationRequestCustomizer(new WechatAuthorizationRequestCustomizer());
//        return defaultOAuth2AuthorizationRequestResolver;
//    }
//
//    /**
//     * 信登录适配，添加自定义token请求参数处理
//     */
//    private OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient() {
//        DefaultAuthorizationCodeTokenResponseClient client = new DefaultAuthorizationCodeTokenResponseClient();
//        client.setRequestEntityConverter(new WechatCodeGrantRequestEntityConverter());
//        // 自定义RestTemplate, 适配微信登录获取token
//        OAuth2AccessTokenResponseHttpMessageConverter messageConverter = new OAuth2AccessTokenResponseHttpMessageConverter();
//        List<MediaType> mediaTypeList = new ArrayList<>(messageConverter.getSupportedMediaTypes());
//        mediaTypeList.add(MediaType.TEXT_PLAIN);
//
//        messageConverter.setAccessTokenResponseConverter(new WechatAccessTokenResponseConverter());
//        messageConverter.setSupportedMediaTypes(mediaTypeList);
//
//        // 初始化Rest Template
//        FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
//        RestTemplate restTemplate = new RestTemplate(Arrays.asList(formHttpMessageConverter, messageConverter));
//
//        restTemplate.setErrorHandler(new OAuth2ErrorResponseErrorHandler());
//        client.setRestOperations(restTemplate);
//        return client;
//    }
//
//}
