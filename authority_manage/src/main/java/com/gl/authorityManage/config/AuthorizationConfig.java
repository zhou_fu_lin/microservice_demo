package com.gl.authorityManage.config;

import cn.hutool.core.util.StrUtil;
import com.gl.authorityCommon.constants.ConfigConstant;
import com.gl.authorityCommon.constants.RedisConstant;
import com.gl.authorityCommon.constants.SecurityConstant;
//import com.gl.authorityManage.authorization.device.DeviceClientAuthenticationConverter;
//import com.gl.authorityManage.authorization.device.DeviceClientAuthenticationProvider;
import com.gl.authorityManage.authorization.handler.*;
//import com.gl.authorityManage.authorization.sms.SmsCaptchaGrantAuthenticationConverter;
//import com.gl.authorityManage.authorization.sms.SmsCaptchaGrantAuthenticationProvider;
import com.gl.authorityManage.authorization.federation.FederatedTokenCustomizer;
//import com.gl.authorityManage.authorization.wechat.WechatAccessTokenResponseConverter;
//import com.gl.authorityManage.authorization.wechat.WechatAuthorizationRequestCustomizer;
//import com.gl.authorityManage.authorization.wechat.WechatCodeGrantRequestEntityConverter;
import com.gl.authorityManage.component.RedisOperator;
import com.gl.authorityManage.properties.CustomSecurityProperties;
import com.gl.authorityManage.utils.SecurityUtil;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CorsFilter;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.*;

/**
 * 认证配置
 * @EnableWebSecurity注解:
 * 1. 加载了WebSecurityConfiguration配置类, 配置安全认证策略
 * 2. 加载了AuthenticationConfiguration, 配置了认证信息
 * @EnableMethodSecurity注解:
 * @EnableGlobalMethodSecurity注解:
 * 1. 简介: 开启Spring环境的方法级安全
 * 2. 参数说明:
 *      1). 开启 prePostEnabled 可以使用 @PreAuthorize 和 @PostAuthorize 两个注解;
 *          (1). @PreAuthorize注解会在方法执行前进行验证，支持Spring EL表达式；
 *          (2). @PostAuthorize注解会在方法执行后进行验证，适用于验证带有返回值的权限;
 *          (3). Spring EL提供了return Object，用于能够在表达式语言中获取返回的对象信息；
 *      2). 开启 securedEnabled 可以使用 @Secured 注解;
 *          (1). 用来定义业务方法的安全配置，在调用的接口或方法上使用该注解;
 *          (2). 在需要安全控制(一般使用角色或者权限进行控制)的方法上指定@Secured，达到只有具备那些角色/权限的用户才可以访问该方法;
 *          (3). 指定角色时必须以ROLE_开头，不可省略；
 *          (4). 不支持Spring EL表达式；
 *          (5). 如果想要使用@Secured注解指定"AND"条件;
 *          (6). 即: 调用deleteAll方法需同时拥有ADMIN和DBA角色的用户时，@Secured便不能实现，只能使用@PreAuthorize/@PostAuthorize注解。
 * @EnableGlobalAuthentication注解:
 *
 * 自定义异常响应三种方法：
 * 1. 实现AuthenticationEntryPoint和AccessDeniedHandler 或他们的子类，并将其实例添加至框架中
 * 2. 在需要的地方，通过一个匿名类来实现自定义处理(比如：在authorizationServerSecurityFilterChain的.jwt()后面添加.accessDeniedHandler和authenticationEntryPoint)
 * 3. 写一个工具方法，通过Method references的方式引用(详情可以看defaultSecurityFilterChain的.jwt()后面添加的方法)
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(jsr250Enabled = true, securedEnabled = true)
public class AuthorizationConfig {

    @Autowired
    private CorsFilter corsFilter;

    @Autowired
    private RedisOperator<String> redisOperator;

    @Autowired
    private CustomSecurityProperties customSecurityProperties;

    /**
     * 配置端点的过滤器链
     */
    @Bean
    public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http, RegisteredClientRepository registeredClientRepository, AuthorizationServerSettings authorizationServerSettings) throws Exception {
        /* 配置默认的设置，忽略认证端点的csrf校验 */
        OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(http);

        SecurityUtil.applyBasicSecurity(http, corsFilter, customSecurityProperties);

//        /* 新建设备码converter和provider */
//        DeviceClientAuthenticationConverter deviceClientAuthenticationConverter = new DeviceClientAuthenticationConverter(authorizationServerSettings.getDeviceAuthorizationEndpoint());
//        DeviceClientAuthenticationProvider deviceClientAuthenticationProvider = new DeviceClientAuthenticationProvider(registeredClientRepository);
        http
            .getConfigurer(OAuth2AuthorizationServerConfigurer.class)
            /* 设置自定义用户确认授权页 */
            .authorizationEndpoint((authorizationEndpointCustomizer) -> {
                /* 校验授权确认页面是否为完整路径；是否是前后端分离的页面 */
                boolean absoluteUrl = UrlUtils.isAbsoluteUrl(customSecurityProperties.getConsentPageUri());
                /* 如果是分离页面则重定向，否则转发请求 */
                authorizationEndpointCustomizer.consentPage(absoluteUrl ? ConfigConstant.CUSTOM_CONSENT_PAGE_URI : customSecurityProperties.getConsentPageUri());
                if (absoluteUrl) {
                    authorizationEndpointCustomizer.errorResponseHandler(new ConsentAuthenticationFailureHandler(customSecurityProperties.getConsentPageUri()));
                    authorizationEndpointCustomizer.authorizationResponseHandler(new ConsentAuthorizationResponseHandler(customSecurityProperties.getConsentPageUri()));
                }
            })
//            /* 设置设备码验证页 */
//            .deviceAuthorizationEndpoint(deviceAuthorizationEndpointCustomizer -> deviceAuthorizationEndpointCustomizer.verificationUri(
//                UrlUtils.isAbsoluteUrl(customSecurityProperties.getDeviceActivatedUri()) ? ConfigConstant.CUSTOM_DEVICE_ACTIVATE_URI : customSecurityProperties.getDeviceActivateUri()
//            ))
//            /* 设置验证设备码用户授权页 */
//            .deviceVerificationEndpoint((deviceVerificationEndpointCustomizer) -> {
//                /* 校验授权确认页面是否为完整路径；是否是前后端分离的页面 */
//                boolean absoluteUrl = UrlUtils.isAbsoluteUrl(customSecurityProperties.getConsentPageUri());
//                deviceVerificationEndpointCustomizer.consentPage(absoluteUrl ? ConfigConstant.CUSTOM_CONSENT_PAGE_URI : customSecurityProperties.getConsentPageUri());
//                /* 如果是分离页面则重定向，否则转发请求 */
//                if (absoluteUrl) {
//                    deviceVerificationEndpointCustomizer.errorResponseHandler(new ConsentAuthenticationFailureHandler(customSecurityProperties.getConsentPageUri()));
//                }
//                /* 如果授权码验证页面或者授权确认页面是前后端分离的 */
//                if (UrlUtils.isAbsoluteUrl(customSecurityProperties.getDeviceActivateUri()) || absoluteUrl) {
//                    deviceVerificationEndpointCustomizer.deviceVerificationResponseHandler(new DeviceAuthorizationResponseHandler(customSecurityProperties.getDeviceActivatedUri()));
//                }
//            })
//            /* 客户端认证添加设备码的转换器(converter)和提供者(provider) */
//            .clientAuthentication((clientAuthenticationCustomizer) -> {
//                clientAuthenticationCustomizer.authenticationConverter(deviceClientAuthenticationConverter);
//                clientAuthenticationCustomizer.authenticationProvider(deviceClientAuthenticationProvider);
//            })
            /* 开启OpenID Connect1.0协议相关端口 */
            .oidc(Customizer.withDefaults());

//        /* 使用Redis储存，读取登录认证信息 */
//         http.securityContext(securityContextCustomizer -> securityContextCustomizer.securityContextRepository(redisSecurityContextRepository));

//        /* 自定义短信认证登录转换器 */
//        SmsCaptchaGrantAuthenticationConverter smsCaptchaGrantAuthenticationConverter = new SmsCaptchaGrantAuthenticationConverter();
//        /* 自定义短信认证登录提供者 */
//        SmsCaptchaGrantAuthenticationProvider smsCaptchaGrantAuthenticationProvider = new SmsCaptchaGrantAuthenticationProvider();
//        http
//            .getConfigurer(OAuth2AuthorizationServerConfigurer.class)
//            /* 认证服务添加元数据 */
//            .authorizationServerMetadataEndpoint((authorizationServerMetadataEndpointCustomizer) -> {
//                authorizationServerMetadataEndpointCustomizer.authorizationServerMetadataCustomizer((authorizationServerMetadataCustomizer) -> {
//                            authorizationServerMetadataCustomizer.grantType(SecurityConstant.GRANT_TYPE_SMS_CODE);
//                });
//            })
//            /* 添加自定义GRANT_TYPE(短信认证登录) */
//            .tokenEndpoint((tokenEndpointCustomizer) -> {
//                tokenEndpointCustomizer.accessTokenRequestConverter(smsCaptchaGrantAuthenticationConverter);
//                tokenEndpointCustomizer.authenticationProvider(smsCaptchaGrantAuthenticationProvider);
//            });

        DefaultSecurityFilterChain defaultSecurityFilterChain = http.build();
//        /* 从框架获取PROVIDER需要的BEAN(如果在获取BEAN之后, HTTP对象才进行BUILD，那下列对象能获取吗？) */
//        OAuth2TokenGenerator<?> oAuth2TokenGenerator = http.getSharedObject(OAuth2TokenGenerator.class);
//        AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
//        OAuth2AuthorizationService oAuth2AuthorizationService = http.getSharedObject(OAuth2AuthorizationService.class);
        /**
         * 以上三个BEAN在build()方法之后调用是因为调用build方法时框架会尝试获取这些类，
         * 如果获取不到则初始化一个实例放入SharedObject中，所以要在build方法调用之后获取
         * 在通过set方法设置进provider中，但是如果在build方法之后调用authenticationProvider(provider)
         * 框架会提示unsupported_grant_type，因为已经初始化完了，在添加就不会生效了
         */
//        smsCaptchaGrantAuthenticationProvider.setOAuth2TokenGenerator(oAuth2TokenGenerator);
//        smsCaptchaGrantAuthenticationProvider.setAuthenticationManager(authenticationManager);
//        smsCaptchaGrantAuthenticationProvider.setOAuth2AuthorizationService(oAuth2AuthorizationService);
        return defaultSecurityFilterChain;
    }


    /**
     * 配置认证相关的过滤器链
     */
    @Bean
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http, ClientRegistrationRepository clientRegistrationRepository) throws Exception {
        SecurityUtil.applyBasicSecurity(http, corsFilter, customSecurityProperties);

        http
//            /* 放行静态资源及特定资源 */
//             .authorizeHttpRequests((authorizeHttpRequestsCustomizer) -> {
//                authorizeHttpRequestsCustomizer.requestMatchers(ConfigConstant.WEB_IGNORE_URL).permitAll();
//                authorizeHttpRequestsCustomizer.anyRequest().authenticated();
//             })
            /* 表单登录处理授权服务器过滤器链的重定向 */
            .formLogin((formLoginCustomizer) -> {
                /* 登录成功和失败改为写回json，不再重定向；兼容前后端不分离方式 */
                formLoginCustomizer.loginPage(ConfigConstant.LOGIN_URL);
                if (UrlUtils.isAbsoluteUrl(customSecurityProperties.getLoginUrl())) {
                    formLoginCustomizer.successHandler(new LoginSuccessHandler());
                    formLoginCustomizer.failureHandler(new LoginFailureHandler());
                }
            })
            /* 联合登录配置 */
            .oauth2Login((oauth2LoginCustomizer) -> {
                oauth2LoginCustomizer.loginPage(customSecurityProperties.getLoginUrl());
//                /* 添加微信辅助工具 */
//                oauth2LoginCustomizer.authorizationEndpoint(
//                        authorizationEndpointCustomizer -> authorizationEndpointCustomizer.authorizationRequestResolver(this.authorizationRequestResolver(clientRegistrationRepository))
//                );
//                oauth2LoginCustomizer.tokenEndpoint(
//                        tokenEndpointCustomizer -> tokenEndpointCustomizer.accessTokenResponseClient(this.accessTokenResponseClient())
//                );
            });
//        /* 使用Redis储存，读取登录认证信息 */
//         http.securityContext(securityContextCustomizer -> securityContextCustomizer.securityContextRepository(redisSecurityContextRepository));
        return http.build();
    }

    /**
     * 自定义JWT, 将权限信息放至JWT中
     */
    @Bean
    public OAuth2TokenCustomizer<JwtEncodingContext> oAuth2TokenCustomizer() {
        return new FederatedTokenCustomizer();
    }

    /**
     * 自定义JWT解析器，设置解析出来的权限信息的前缀与在JWT中的KEY(移动到资源服务)
     */
    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        /* 设置权限信息在JWT的CLAIMS的KEY */
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName(SecurityConstant.AUTHORITIES_KEY);
        /* 设置解析权限信息的前缀，设置为空是去掉前缀 */
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("");

        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }

    /**
     * 将AuthenticationManager注入ioc中，其它需要使用地方可以直接从ioc中获取
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) {
        try {
            return authenticationConfiguration.getAuthenticationManager();
        } catch (Exception e) {
            throw new RuntimeException("获取AuthenticationManager对象失败: " + e.getMessage());
        }
    }

    /**
     * 配置密码解析器，使用BCrypt的方式对密码进行加密和验证
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 配置JWK源
     */
    @Bean
    public JWKSource<SecurityContext> jwkSource() {
        // 读取redis是否存在缓存
        String jwkSetCache = redisOperator.get(RedisConstant.AUTHORIZATION_JWS_PREFIX_KEY);
        try {
            if (StrUtil.isNotBlank(jwkSetCache)) {
                /* 解析存储的jws */
                JWKSet jwkSet = JWKSet.parse(jwkSetCache);
                return new ImmutableJWKSet<>(jwkSet);
            }
            /* 生成RSA密钥，提供给JWK */
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            /* 使用非对称加密，公开用于检索匹配指定JWK方法 */
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            RSAKey rsaKey = new RSAKey.Builder(publicKey).privateKey(privateKey).keyID(uuid).build();
            // 生成JWK
            JWKSet jwkSet = new JWKSet(rsaKey);
            // 转为json字符串
            String jwkSetStr = jwkSet.toString(Boolean.FALSE);
            // 存入redis
            redisOperator.set(RedisConstant.AUTHORIZATION_JWS_PREFIX_KEY, jwkSetStr);
            return new ImmutableJWKSet<SecurityContext>(jwkSet);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * 配置JWT解析器
     */
    @Bean
    public JwtDecoder jwtDecoder(JWKSource<SecurityContext> jwkSource) {
        return OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource);
    }


    /**
     * 添加认证服务配置，设置JWT签发者、默认端点请求地址等
     */
    @Bean
    public AuthorizationServerSettings authorizationServerSettings() {
        return AuthorizationServerSettings.builder().issuer(customSecurityProperties.getIssuerUrl()).build();
    }

//    /**
//     * 配置客户端信息储存库(基于内存，只要创建InMemoryRegisteredClientRepository对象即可)
//     * 该配置是读取数据库认证
//     * 认证信息放至redis中需要屏蔽该配置
//     */
//    @Bean
//    public RegisteredClientRepository registeredClientRepository(JdbcTemplate jdbcTemplate) {
//        return new JdbcRegisteredClientRepository(jdbcTemplate);
//    }
//
//    /**
//     * 配置基于数据库的OAUTH2授权管理服务
//     * 该配置是读取数据库认证
//     * 认证信息放至redis中需要屏蔽该配置
//     */
//    @Bean
//    public OAuth2AuthorizationService authorizationService(JdbcTemplate jdbcTemplate, RegisteredClientRepository registeredClientRepository) {
//        /* 基于数据库的OAUTH2认证服务(另一种基于内存实现的认证服务-InMemoryOAuth2AuthorizationService) */
//        return new JdbcOAuth2AuthorizationService(jdbcTemplate, registeredClientRepository);
//    }
//
//    /**
//     * 配置基于数据库的授权确认管理服务
//     * 该配置是读取数据库认证
//     * 认证信息放至redis中需要屏蔽该配置
//     */
//    @Bean
//    public OAuth2AuthorizationConsentService authorizationConsentService(JdbcTemplate jdbcTemplate, RegisteredClientRepository registeredClientRepository) {
//        /* 基于数据库的授权确认管理服务(另一种基于内存实现的认证服务-InMemoryOAuth2AuthorizationConsentService) */
//        return new JdbcOAuth2AuthorizationConsentService(jdbcTemplate, registeredClientRepository);
//    }

    /**
     * AuthorizationRequest 自定义配置
     */
//    private OAuth2AuthorizationRequestResolver authorizationRequestResolver(ClientRegistrationRepository clientRegistrationRepository) {
//        DefaultOAuth2AuthorizationRequestResolver defaultOAuth2AuthorizationRequestResolver = new DefaultOAuth2AuthorizationRequestResolver(clientRegistrationRepository, OAuth2AuthorizationRequestRedirectFilter.DEFAULT_AUTHORIZATION_REQUEST_BASE_URI);
//        // 兼容微信登录授权申请
//        defaultOAuth2AuthorizationRequestResolver.setAuthorizationRequestCustomizer(new WechatAuthorizationRequestCustomizer());
//        return defaultOAuth2AuthorizationRequestResolver;
//    }

    /**
     * 信登录适配，添加自定义token请求参数处理
     */
//    private OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient() {
//        DefaultAuthorizationCodeTokenResponseClient client = new DefaultAuthorizationCodeTokenResponseClient();
//        client.setRequestEntityConverter(new WechatCodeGrantRequestEntityConverter());
//        // 自定义RestTemplate, 适配微信登录获取token
//        OAuth2AccessTokenResponseHttpMessageConverter messageConverter = new OAuth2AccessTokenResponseHttpMessageConverter();
//        List<MediaType> mediaTypeList = new ArrayList<>(messageConverter.getSupportedMediaTypes());
//        mediaTypeList.add(MediaType.TEXT_PLAIN);
//
//        messageConverter.setAccessTokenResponseConverter(new WechatAccessTokenResponseConverter());
//        messageConverter.setSupportedMediaTypes(mediaTypeList);
//
//        // 初始化Rest Template
//        FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
//        RestTemplate restTemplate = new RestTemplate(Arrays.asList(formHttpMessageConverter, messageConverter));
//
//        restTemplate.setErrorHandler(new OAuth2ErrorResponseErrorHandler());
//        client.setRestOperations(restTemplate);
//        return client;
//    }

}
