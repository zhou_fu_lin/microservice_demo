//package com.gl.authorityResource.config;
//
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.filter.CorsFilter;
//
//import java.util.Collections;
//
//@Configuration
//@EnableConfigurationProperties
//public class BeanConfig {
//
//    @Bean
//    public CorsFilter corsFilter() {
//        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        // 1 允许任何来源
//        // 设置允许跨域的域名,如果允许携带cookie的话,路径就不能写*号, *表示所有的域名都可以跨域访问(开放下面代码，需要注释掉"1 允许任何来源"下面的代码)
//        corsConfiguration.setAllowedOriginPatterns(Collections.singletonList("*"));
//        // 2 允许任何请求头
//        corsConfiguration.addAllowedHeader(CorsConfiguration.ALL);
//        // 3 允许任何方法
//        corsConfiguration.addAllowedMethod(CorsConfiguration.ALL);
//        // 4 设置跨域访问可以携带cookie
//        corsConfiguration.setAllowCredentials(true);
//        // 给配置源对象设置过滤的参数
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", corsConfiguration);
//        return new CorsFilter(source);
//    }
//
//}
