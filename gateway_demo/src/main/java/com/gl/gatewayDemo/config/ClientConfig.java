//package com.gl.gatewayDemo.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
//import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//
//import java.util.Collection;
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//
///**
// * 客户端配置
// */
//@Configuration
//public class ClientConfig {
//
//    /**
//     * 解析用户权限信息（当在浏览器中直接访问接口，框架自动调用OIDC流程登录时会用到该配置）
//     */
//    @Bean
//    public GrantedAuthoritiesMapper userAuthoritiesMapper() {
//        return (authorities) -> {
//            Set<GrantedAuthority> mappedAuthorities = new HashSet<>();
//
//            authorities.forEach(authority -> {
//                Object userAuthorities = null;
//                if (authority instanceof OAuth2UserAuthority oauth2UserAuthority) {
//                    // 从认证服务获取的用户信息中提取权限信息
//                    Map<String, Object> userAttributes = oauth2UserAuthority.getAttributes();
//                    userAuthorities = userAttributes.get("authorities");
//                }
//                if (userAuthorities instanceof Collection<?> collection) {
//                    // 转为SimpleGrantedAuthority的实例并插入mappedAuthorities中
//                    collection.stream()
//                            .filter(a -> a instanceof String)
//                            .map(String::valueOf)
//                            .map(SimpleGrantedAuthority::new)
//                            .forEach(mappedAuthorities::add);
//                }
//            });
//
//            return mappedAuthorities;
//        };
//    }
//
//}
