package com.gl.gatewayDemo.config;

import com.gl.authorityCommon.constants.ConfigConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@EnableWebFluxSecurity
public class ResourceConfig {

    @Bean
    public SecurityWebFilterChain defaultSecurityFilterChain(ServerHttpSecurity http) {
        http.authorizeExchange((exchange) -> {
                exchange.pathMatchers(ConfigConstant.WEB_FLUX_IGNORE_URL).permitAll();
                exchange.anyExchange().authenticated();
            })
            .csrf(ServerHttpSecurity.CsrfSpec::disable)
            .cors(ServerHttpSecurity.CorsSpec::disable)
            // 开启OAuth2登录
            .oauth2Login(Customizer.withDefaults());

        return http.build();
    }

}
