package com.gl.authorityClient.contoller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

    @GetMapping("/client1")
    @PreAuthorize("hasAuthority('SCOPE_message.read')")
    public String getClient1() {
        return "client01";
    }

    @GetMapping("/client2")
    @PreAuthorize("hasAuthority('SCOPE_message.write')")
    public String getClient2() {
        return "client02";
    }

    @GetMapping("/app")
    @PreAuthorize("hasAuthority('app')")
    public String app() {
        return "app";
    }

}
