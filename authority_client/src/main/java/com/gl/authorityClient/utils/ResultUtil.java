package com.gl.authorityClient.utils;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.time.LocalDateTime;

@Data
@Setter(AccessLevel.NONE)
public class ResultUtil {

    /**
     * 返回码
     */
    private Integer code;

    /**
     * 数据
     */
    private Object data;

    /**
     * 信息
     */
    private Object msg;

    /**
     * 时间
     */
    private LocalDateTime time;

    public ResultUtil(Integer code, Object data) {
        this.time = LocalDateTime.now();
        this.code = code;
        this.data = data;
    }

    public ResultUtil(Integer code, Object data, Object msg) {
        this.time = LocalDateTime.now();
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

}
