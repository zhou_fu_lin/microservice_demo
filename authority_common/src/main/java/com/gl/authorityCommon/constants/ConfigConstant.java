package com.gl.authorityCommon.constants;

public class ConfigConstant {

    public static final String CUSTOM_DEVICE_ACTIVATE_URI = "/activate";

    public static final String CUSTOM_CONSENT_PAGE_URI = "/oauth2/consent/redirect";

    public static final String DEVICE_VERIFICATION_URI = "/oauth2/device_verification";

    public static final String LOGIN_URL = "/login";

    public static final String ERROR_URI_3_2_1 = "https://datatracker.ietf.org/doc/html/rfc6749#section-3.2.1";

    public static final String ERROR_URI_5_2 = "https://datatracker.ietf.org/doc/html/rfc6749#section-5.2";

    public static final String[] WEB_FLUX_IGNORE_URL = {
            "/authorityResource/common/**",
    };

    public static final String[] WEB_IGNORE_URL = {
            "/common/**",
            "/login",
            "/error"
    };

}
