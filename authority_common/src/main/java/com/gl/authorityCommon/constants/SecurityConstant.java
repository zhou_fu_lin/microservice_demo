package com.gl.authorityCommon.constants;

public class SecurityConstant {

    /**
     * 自定义 grant type —— 短信验证码
     */
    public static final String GRANT_TYPE_SMS_CODE = "urn:ietf:params:oauth:grant-type:sms_code";

    /**
     * 自定义 grant type —— 短信验证码 —— 手机号的字段名
     */
    public static final String OAUTH_PARAMETER_NAME_PHONE = "phone";

    /**
     * 自定义 grant type —— 短信验证码KEY
     */
    public static final String OAUTH_PARAMETER_NAME_SMS_CAPTCHA = "sms_captcha";

    /**
     * 自定义 grant type —— 图形验证码KEY
     */
    public static final String OAUTH_PARAMETER_NAME_GRAPH_CAPTCHA = "graph_captcha";

    /**
     * 登录方式入参名
     */
    public static final String LOGIN_TYPE_NAME = "loginType";

    public static final String LOGIN_TYPE_SMS = "sms";

    public static final String LOGIN_TYPE_PASSWORD = "password";

    /**
     * 第三方登录方式 —— Gitee
     */
    public static final String THIRD_LOGIN_GITEE = "gitee";

    /**
     * 第三方登录方式 —— Github
     */
    public static final String THIRD_LOGIN_GITHUB = "github";

    /**
     * 第三方登录类型 —— 微信
     */
    public static final String THIRD_LOGIN_WECHAT = "wechat";

    /**
     * 微信登录相关参数 —— appid：微信的应用id
     */
    public static final String WECHAT_PARAMETER_APPID = "appid";

    /**
     * 微信登录相关参数 —— openid：用户唯一id
     */
    public static final String WECHAT_PARAMETER_OPENID = "openid";

    /**
     * 微信登录相关参数 —— secret：微信的应用秘钥
     */
    public static final String WECHAT_PARAMETER_SECRET = "secret";

    /**
     * 微信登录相关参数 —— forcePopup：强制此次授权需要用户弹窗确认
     */
    public static final String WECHAT_PARAMETER_FORCE_POPUP = "forcePopup";

    /**
     * 验证码ID入参名
     */
    public static final String CAPTCHA_ID_NAME = "captchaId";

    /**
     * 获取验证码值入参名
     */
    public static final String CAPTCHA_CODE_NAME = "captchaCode";

    /**
     * TOKEN中权限的字段名
     */
    public static final String AUTHORITIES_KEY = "authorities";

    /**
     * 随机字符串请求头名字
     */
    public static final String NONCE_ID_HEADER_NAME = "nonceId";

    /**
     * 前后端分离认证服务参数的字段名
     */
    public static final String TARGET_HEADER_NAME = "target";

    /**
     * 前后端分离认证服务参数的字段名
     */
    public static final String LOGIN_PAGE_HEADER_NAME = "loginPage";

}
