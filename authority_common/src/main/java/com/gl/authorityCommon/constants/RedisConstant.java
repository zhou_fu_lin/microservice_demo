package com.gl.authorityCommon.constants;

public class RedisConstant {

    /**
     * 短信验证码前缀
     */
    public static final String SMS_CAPTCHA_PREFIX_KEY = "sms_captcha:";

    /**
     * 图形验证码前缀
     */
    public static final String GRAPH_CAPTCHA_PREFIX_KEY = "graph_captcha:";

    /**
     * 二维码信息前缀
     */
    public static final String QR_CODE_PREV = "qrcode:";

    /**
     * 认证信息存储前缀
     */
    public static final String SECURITY_CONTEXT_PREFIX_KEY = "security_context:";

    /**
     * JWK缓存前缀
     */
    public static final String AUTHORIZATION_JWS_PREFIX_KEY = "authorization_jws";

    /**
     * 验证码过期时间，默认五分钟
     */
    public static final long CAPTCHA_TIMEOUT_SECONDS = 60L * 5;

    /**
     * 二维码过期时间
     */
    public static final long QR_CODE_INFO_TIMEOUT = 60 * 10;

    /**
     * 默认十分钟
     */
    public static final long DEFAULT_TIMEOUT_SECONDS = 60L * 10;

}
