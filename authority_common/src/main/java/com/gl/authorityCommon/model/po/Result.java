package com.gl.authorityCommon.model.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {

    /**
     * 响应状态码
     */
    private Integer code;

    /**
     * 响应信息
     */
    private String message;

    /**
     * 接口是否处理成功
     */
    private Boolean state;

    /**
     * 接口响应时携带的数据
     */
    private T data;

    public Result() {}

    public Result(Integer code, String message, Boolean state, T data) {
        this.code = code;
        this.state = state;
        this.message = message;
        this.data = data;
    }

}
