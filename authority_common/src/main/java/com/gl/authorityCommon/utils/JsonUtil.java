package com.gl.authorityCommon.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

@Slf4j
public class JsonUtil {

    private static ObjectMapper mapper;

    public static ObjectMapper getInitializeMapper() {
        if (JsonUtil.mapper == null) {
            synchronized (ObjectMapper.class) {
                if (JsonUtil.mapper == null) {
                    JsonUtil.mapper = new ObjectMapper();
                    // 对象的所有字段全部列入，还是其他的选项，可以忽略null等
                    JsonUtil.mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
                    // 取消默认的时间转换为timeStamp格式
                    JsonUtil.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
                    // 设置Date类型的序列化及反序列化格式
                    JsonUtil.mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                    // 忽略空Bean转json的错误
                    JsonUtil.mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                    // 忽略未知属性，防止json字符串中存在，java对象中不存在对应属性的情况出现错误
                    JsonUtil.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                }
            }
        }
        return JsonUtil.mapper;
    }

    /**
     * JSON字符串转对象
     */
    public static <T> T  jsonConvertToObject(String json, Class<T> clazz) {
        if (json == null || clazz == null) {
            return null;
        }
        try {
            ObjectMapper objectMapper = JsonUtil.getInitializeMapper();
            return objectMapper.readValue(json, clazz);
        } catch (IOException e) {
            log.error("JSON转换失败, 原因: ", e);
        }
        return null;
    }

    /**
     * JSON字符串转为对象
     */
    public static <T> T jsonCovertToObject(String json, TypeReference<T> type) {
        if (json == null || type == null) {
            return null;
        }
        try {
            ObjectMapper objectMapper = JsonUtil.getInitializeMapper();
            return objectMapper.readValue(json, type);
        } catch (IOException e) {
            log.error("JSON转换失败, 原因: ", e);
        }
        return null;
    }

    /**
     * 将流中的数据转为java对象
     */
    public static <T> T covertStreamToObject(InputStream inputStream, Class<T> clazz) {
        if (inputStream == null || clazz == null) {
            return null;
        }
        try {
            ObjectMapper objectMapper = JsonUtil.getInitializeMapper();
            return objectMapper.readValue(inputStream, clazz);
        } catch (IOException e) {
            log.error("JSON转换失败, 原因: ", e);
        }
        return null;
    }

    /**
     * json字符串转为复杂类型List
     */
    public static <T> T jsonCovertToObject(String json, Class<?> collectionClazz, Class<?> ... elementsClazz) {
        if (json == null || collectionClazz == null || elementsClazz == null) {
            return null;
        }
        try {
            ObjectMapper objectMapper = JsonUtil.getInitializeMapper();
            JavaType javaType = objectMapper.getTypeFactory().constructParametricType(collectionClazz, elementsClazz);
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            log.error("JSON转换失败, 原因: ", e);
        }
        return null;
    }

    /**
     * 对象转为JSON字符串
     */
    public static String objectCovertToJson(Object o) {
        if (o == null) {
            return null;
        }
        try {
            if (o instanceof String) {
                return (String) o;
            }
            ObjectMapper objectMapper = JsonUtil.getInitializeMapper();
            return objectMapper.writeValueAsString(o);
        } catch (IOException e) {
            log.error("JSON转换失败, 原因: ", e);
        }
        return null;
    }

    /**
     * 将对象转为另一个对象(两个对象结构要一致, 比如: Object转为具体的对象)
     */
    public static  <T> T objectCovertToObject(Object o, Class<?> collectionClazz, Class<?>... elementsClazz) {
        String json = objectCovertToJson(o);
        return JsonUtil.jsonCovertToObject(json, collectionClazz, elementsClazz);
    }

}
